import os
from distutils.dir_util import copy_tree, remove_tree
import logging
# import logging_util
logger = logging.getLogger(__name__)


class Local:
    @staticmethod
    def get_absolute_path(file_url):
        return os.path.abspath(file_url)

    @staticmethod
    def write(file_url, file_data):
        os.makedirs(os.path.dirname(file_url), exist_ok=True)
        with open(file_url, 'wb') as file:
            file.write(file_data)

    @staticmethod
    def read(file_url):
        with open(file_url, 'rb') as file:
            return file.read()

    @staticmethod
    def move(src_url, dst_url):
        os.makedirs(os.path.dirname(dst_url), exist_ok=True)
        os.rename(src_url, dst_url)

    @staticmethod
    def delete(file_url):
        os.remove(file_url)

    @staticmethod
    def file_exists(file_url):
        return os.path.isfile(file_url)

    @staticmethod
    def get_files(dir_url, recursive=False):
        if os.path.exists(dir_url):
            if not recursive:
                return [entry for entry in os.listdir(dir_url) if os.path.isfile(os.path.join(dir_url, entry))]

            files = []
            for dir_, _, files_ in os.walk(dir_url):
                rel_dir = "" if dir_ == dir_url else os.path.relpath(dir_, dir_url)
                for name in files_:
                    files.append(os.path.join(rel_dir, name))
            return files
        else:
            return []

    @staticmethod
    def is_dir(url):
        return os.path.isdir(url)

    @staticmethod
    def get_dirs(dir_url):
        if os.path.exists(dir_url):
            return [entry for entry in os.listdir(dir_url) if os.path.isdir(os.path.join(dir_url, entry))]
        else:
            return []

    @staticmethod
    def clone_dir(src_dir, dst_dir):
        copy_tree(src_dir, dst_dir)

    @staticmethod
    def delete_dir(dir_url):
        remove_tree(dir_url)
