kafka_log_file = 'logs/kafka_logs.log'
bootstrap_servers = 'localhost:9092'
partitions = 4  # Any new topic created will have this many partitions
replication_factor = 1
max_request_size = 41943040
max_partition_fetch_bytes = 41943040
consumer_timeout_ms = 1000
auto_offset = 'latest'
workers_producer = 1
workers_consumer = 16

topics = ['retrigger_download', 'ontology', 'corpus', 'download_dump', 'doc_analysis', 'conll_ontology',
          'vocab_analysis', 'corpus_analysis', 'seed_document']
