import enum


class PhaseEnum(str, enum.Enum):
    CORPUS_GENERATION = 'corpus_generation'
    LEARNING = 'learning'
    ANALYSIS = 'analysis'
    VALIDATION = 'validation'


class StatusEnum(str, enum.Enum):
    AWAITING = 'awaiting'
    PROCESSING = 'processing'
    REVIEWING = 'reviewing'
    COMPLETED = 'completed'


class GoldenDocumentType(str, enum.Enum):
    RAW = 'raw'
    PLAIN_TEXT = 'plain_text'

class GoldenLinkType(str, enum.Enum):
    PUBLIC = 'public'
    UNPARSED = 'unparsed'
    PARSED = 'parsed'
    SENTENCES = 'sentences'


class LearningType(str, enum.Enum):
    SUPERVISED = 'supervised'
    UNSUPERVISED = 'unsupervised'
    ANALYSIS = 'analysis'
    UNSUPERVISED_WITHOUT_DW = 'unsupervised_without_download'
    UNSUPERVISED_WITH_DW = 'unsupervised_with_download'
    ALL = 'all'


class SupervisedLearningType(str, enum.Enum):
    category = 'category'
    entity = 'entity'


class AttributeEnum(enum.Enum):
    # Counters
    DOWNLOAD = 'download'
    MSG_COUNT = 'message_count'
    MSG_RECEIVED = 'message_received'
    DOCUMENT_COUNT = 'document_count'
    SEED_DOCUMENT_COUNT = 'seed_document_count'
    DEFINITION_DOCUMENT_COUNT = 'definition_document_count'
    TERM_MESSAGE_COUNT = 'term_message_count'
    CONTEXT_DEF_COUNT = 'context_def_count'
    CONTENT_MSG_COUNT = 'content_msg_count'
    DOWNLOAD_COUNT = 'download_count'
    GOLDEN_DOWNLOAD_COUNT = 'golden_download_count'
    GOLDEN_DOCUMENT_COUNT = 'golden_document_count'
    GOLDEN_DOCUMENTS = 'golden_documents'
    CONTEXT_DOWNLOAD_COUNT = 'context_download_count'
    UNIQUE_FILE_NO = 'unique_file_no'
    CORENLP_RECEIVE_COUNT = 'corenlp_receive_count'
    PHASE_DOC_COUNT = 'phase_doc_count'
    AO_STATS = 'ao_stats'
    AO_PHASE_STATS = 'ao_phase_stats'
    TRIPLES_COUNT = 'triples_count'
    SENTENCE_COUNT = 'sentence_count'
    CORPUS_ITERATION = 'corpus_iteration'
    LEARNING_ITERATION = 'learning_iteration'
    GOLDEN_DOWNLOAD_SIZE = 'golden_download_size'
    CONTEXT_DOWNLOAD_SIZE = 'context_download_size'

    # Processed signals
    PROCESSED = 'processed'
    RUNNING = 'running'
    TERM_PROCESSED = 'term_processed'
    DEFS_PROCESSED = 'defs_processed'
    DOCUMENT_PROCESSED = 'document_processed'
    CONTEXT_PROCESSED = 'context_processed'
    GOLDEN_DOCUMENT_PROCESSED = 'golden_document_processed'
    SEED_DOCUMENT_PROCESSED = 'seed_document_processed'
    DOC_LEVEL_PROCESSED = 'doc_level_processed'
    OUTPUT_PATH = 'output_path'
    MESSAGE_ARRIVED = 'message_arrived'
    FEEDBACK_PROCESSED = 'feedback_processed'
    HAS_SENTENCES = 'has_sentences'

    # Kafka-topics
    INPUT_Q_ONTLOGY = 'input_q_ontology'
    INPUT_Q_CORPUS = 'input_q_corpus'
    INPUT_Q_CORPUS_F = 'input_q_corpusF'
    INPUT_Q = 'input_q'
    INPUT_Q_WORDSPACE = 'input_q_wordspace'
    INPUT_Q_FASTTEXT = 'input_q_fasttext'
    INPUT_Q_FASTTEXT_SUPERVISED = 'input_q_fasttext_supervised'
    INPUT_Q_BERT_SUPERVISED = 'input_q_bert_supervised'
    OUTPUT_Q = 'output_q'
    OUTPUT_Q_CORENLP = 'output_q_coreNLP'
    OUTPUT_Q_SEED = 'output_q_seed_doc'
    OUTPUT_Q_GOLDEN = 'output_q_golden_doc'
    OUTPUT_Q_CONTEXT = 'output_q_context_doc'
    OUTPUT_Q_TRAIN_SUPERVISED = 'output_q_train_supervised'
    OUTPUT_Q_DOC_LEVEL = 'output_q_doc_level'
    OUTPUT_Q_DEFINITIONS = 'output_q_definitions'
    OUTPUT_Q_DOCS_REVIEW = 'output_q_docs_review'
    OUTPUT_Q_TERMS_REVIEW = 'output_q_terms_review'
    INPUT_FILE = 'input_file'
    OTHERS_INPUT = "others_input"
    OTHERS_OUTPUT = "others_output"
    TRAINING_SET = "training_set"
    PREDICTION_SET_INPUT = "prediction_set_input"
    PREDICTION_SET_OUTPUT = "prediction_set_output"
    OUTPUT_VAL = "output_val"
    INPUT_VAL = "input_val"

    # storage locations
    TERM_POOL = 'term_pool'
    CORPUS_DOCUMENTS = 'dir_corpus_documents'
    PARSED_FILES = 'dir_corpus_parsed_files'
    PARSED_FILES_EXTRACTOR = 'dir_corpus_parsed_files_extractor'
    PARSED_JSONS = 'dir_corpus_parsed_jsons'
    GOLDEN_TERMS = 'dir_golden_terms'
    GOLDEN_URL = 'golden_url'
    CONTEXT_TERMS = 'dir_context_terms'
    CONTEXT_DEFS = 'dir_context_defs'
    DOWNLOAD_PATH = 'dir_downloaded_documents'
    DATA_FILE_PATH = 'data_file_path'
    MODEL_STORAGE = 'model_storage'
    OUTPUT = 'output'
    FEEDBACK_DATA = 'feedback_data'
    REVIEW_DATA = 'review_data'
    ENTITY_INPUT = 'entity_input'
    GOLDEN_DOCUMENT_LIST = 'golden_document_list'

    # Generic signals
    DOMAIN = 'domain'
    SUBDOMAIN = 'subdomain'
    NAMESPACE = 'namespace'
    ENVIRONMENT = 'environment'
    DOWNLOAD_DOCUMENT = 'download_document'
    METADATA = 'metadata'
    PHASE = 'phase'
    TERM_CLUSTER = 'term_cluster'
    GOLDEN_TERMS_LOADED = 'golden_terms_loaded'
    GOLDEN_DOCUMENTS_TYPE = 'golden_documents_type'
    SENTENCE_RANKER_AUTO = 'sentence_ranker_auto'
    ENTRY_FLAG = 'entry_flag'
    USER_INPUT_CHANGED = 'user_input_changed'
    FAILED = 'failed'
    REJECTED = 'rejected'
    SELECTED = 'selected'
    CONTEXTS = 'contexts'
    WELL_REPRESENTED_CONTEXTS = 'well_represented_contexts'
    TERM_COUNT = 'term_count'
    STATUS = 'status'
    FEEDBACK_ARRIVED = 'feedback_arrived'

    # Others
    FILE_NAME_LIST = 'file_name_list'
    MODEL = 'model'
    USE_CASE = 'use_case'
    ZK_LOADING = 'zk_loading'
    DIMENSION = 'dimension'
    THRESHOLD = 'threshold'
    LEARNING_TYPE = 'learning_type'
    EVALUATION_RESULT = 'evaluation_result'

class ModuleEnum(enum.Enum):
    AUTO_ONTOLOGY = 'auto_ontology'
    REQUIREMENT_EXTRACTOR = 'requirement_extractor'
    TABLE_EXTRACTOR = 'table_extractor'
    RDF_CREATOR = 'rdf_creator'
    CONFIG = 'config'
    CORE_NLP = 'core_nlp'
    CORPUS_FILTER = 'corpus_filter'
    DOCUMENT_FILTER = 'document_filter'
    DOCUMENT_READER_STORAGE = 'document_reader_storage'
    DOCUMENT_READER_DOWNLOADER = 'document_reader_downloader'
    DOWNLOADER = 'downloader'
    STARTER = 'starter'
    TERM_FILTER = 'term_filter'
    CORPUS_PREPROCESSOR = 'corpus_preprocessor'
    BERT_UNSUPERVISED = 'bert_unsupervised'
    BERT_SUPERVISED = 'bert_supervised'
    FASTTEXT_UNSUPERVISED = 'fasttext_unsupervised'
    FASTTEXT_SUPERVISED = 'fasttext_supervised'
    FASTTEXT_VALIDATOR = 'fasttext_validator'
    WORDSPACE_UNSUPERVISED = 'wordspace_unsupervised'
    WORDSPACE_SUPERVISED = 'wordspace_supervised'
    WORDSPACE_VALIDATOR = 'wordspace_validator'
    VALIDATION_AGGREGATOR = 'validation_aggregator'
    SENTENCE_RANKER = 'sentence_ranker'
    STRUCTURE_EXTRACTOR = 'structure_extractor'
    USER_INPUT = 'user_input'
    ANALYSIS = 'analysis'
    USER_REVIEW = 'user_review'
    DOC_USER_REVIEW = 'doc_user_review'
    TERM_USER_REVIEW = 'term_user_review'
    ONTOLOGY_CURATOR = 'ontology_curator'
    CONTROLLER = 'controller'

