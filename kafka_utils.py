import logging
import json
import time
import sys
import traceback
from kafka import KafkaConsumer, KafkaProducer, KafkaAdminClient, admin
from zookeeper_utils import Zookeeper
from threading import Thread
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from concurrent.futures.process import BrokenProcessPool
import kafka_config as cfg

# import logging_util
logger = logging.getLogger(__name__)


class CallBack:
    def on_success(self, key, value=None): pass

    def on_error(self, key, error_message): pass


class Handler:
    @staticmethod
    def producer_request(topic, key, value, callback):
        try:
            _producer = Kafka.get_producer()
            _producer.send(topic, key=key, value=value).get(timeout=5)
            callback.on_success(key)
        except Exception as e:
            logger.error(f'Error: {traceback.format_exc()}')
            callback.on_error(key, e)

    @staticmethod
    def consumer_request(key, value, callback):
        global process_pool_consumer
        try:
            logger.info(f'handler triggered: {value}')
            callback.on_success(key, value)
        except BrokenProcessPool:
            logger.error('Process pool is broken. Restarting executor.')
            process_pool_consumer.shutdown(wait=True)
            process_pool_consumer = ProcessPoolExecutor(max_workers=cfg.workers_consumer,
                                                        initializer=Zookeeper.refresh_instance)
            process_pool_consumer.submit(Handler.consumer_request, key, value, callback)
        except Exception as e:
            logger.error(f'ERROR : {traceback.format_exc()}')
            callback.on_error(key, e)


class KafkaService:
    @staticmethod
    def send(topic, key, payload, callback: CallBack):
        try:
            # logger.info('kafka_utils: before process_pool')
            # thread_pool.submit(Handler.producer_request, topic, key, payload, callback)
            Handler.producer_request(topic, key, payload, callback)
        except Exception as e:
            print(f'Error while sending data at topic: {topic} to Kafka')
            callback.on_error(key, e)

    @staticmethod
    def receive(topic, consumer_group, callback):
        global t
        if t is None:
            logger.info('The KafkaSystem thread is not running. Starting it now . . .')
            t = KafkaConsumerThread()
            t.start()
        consumer = Kafka.get_consumer(topic, consumer_group)
        consumers.append((consumer, callback))
        print(f'Added consumer of group: {consumer_group} in topic:{topic}; size {len(consumers)}')
        return consumer

    @staticmethod
    def receive_sync(topic, consumer_group, id_, time_out):
        consumer = None
        try:
            consumer = Kafka.get_consumer(topic, consumer_group)
            final_time = time_out + time.time()  # time_out in seconds
            print(final_time)
            while final_time > time.time():
                for msg in consumer:
                    key = msg.key if msg.key is None else msg.key.decode('utf-8')
                    value = msg.value
                    if value and value['id'] == id_:
                        return key, value
        finally:
            if consumer:
                consumer.close()
        raise TimeoutError

    @staticmethod
    def flush():
        Kafka.get_producer().flush()

    @staticmethod
    def receive_cancel(__consumer):
        global consumers
        consumers = [tuple_consumer for tuple_consumer in consumers if tuple_consumer[0] == __consumer]


class KafkaConsumerThread(Thread):
    def __init__(self): super().__init__()

    def run(self):
        global process_pool_consumer
        key = ''
        value = ''
        callback_ = None
        print('Running KafkaThread. . . ')
        try:
            while True:
                for consumer, callback in consumers:
                    callback_ = callback
                    for msg in consumer:
                        key = msg.key if msg.key is None else msg.key.decode('utf-8')
                        value = msg.value
                        if value:
                            process_pool_consumer.submit(Handler.consumer_request, key, value, callback)
        except BrokenProcessPool:
            logger.error('Process pool is broken. Restarting executor.')
            process_pool_consumer.shutdown(wait=True)
            process_pool_consumer = ProcessPoolExecutor(max_workers=cfg.workers_consumer,
                                                        initializer=Zookeeper.refresh_instance)
            process_pool_consumer.submit(Handler.consumer_request, key, value, callback_)

        except Exception:
                logger.error(f'Error occurred in {__name__} : {sys.exc_info()[0]}', exc_info=True)
                raise


class Kafka:
    __producer = None

    @staticmethod
    def create_topic():
        topic_list = cfg.topics
        topics = Kafka.get_topics()
        topic_list = list(set(topic_list) - set(topics))
        try:
            admin_client = KafkaAdminClient(bootstrap_servers=cfg.bootstrap_servers)
            topic_list = [admin.NewTopic(name=topic, num_partitions=cfg.partitions,
                                         replication_factor=cfg.replication_factor) for topic in topic_list]
            admin_client.create_topics(topic_list)
            logger.info(f'Topic {topic_list} creatad successfully')
        except Exception:
            logging.error(f' ERROR while creating new topic {sys.exc_info()[0]}', exc_info=True)
            raise

    @staticmethod
    def get_producer():
        Kafka.__producer = KafkaProducer(bootstrap_servers=cfg.bootstrap_servers,
                                         value_serializer=lambda x: json.dumps(x).encode('utf-8'),
                                         max_request_size=cfg.max_request_size)
        return Kafka.__producer

    @staticmethod
    def get_consumer(topic, group):
        return KafkaConsumer(topic, bootstrap_servers=cfg.bootstrap_servers, group_id=group,
                             consumer_timeout_ms=cfg.consumer_timeout_ms, enable_auto_commit=True,
                             auto_offset_reset=cfg.auto_offset,
                             value_deserializer=lambda x: json.loads(x.decode('utf-8')),
                             max_partition_fetch_bytes=cfg.max_partition_fetch_bytes)

    @staticmethod
    def get_topics():
        try:
            return KafkaConsumer().topics()
        except Exception as e:
            return e

    @staticmethod
    def get_request_count():
        return f'Total requests at: {time.ctime()} is: {len(consumers)}'


# thread_pool = ThreadPoolExecutor(max_workers=cfg.workers_producer)
process_pool_consumer = ProcessPoolExecutor(max_workers=cfg.workers_consumer, initializer=Zookeeper.refresh_instance)
consumers = []
t = None
