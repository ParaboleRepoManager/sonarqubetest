import os
import logging
import traceback
import smtplib
import ssl
import email_config

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
logger = logging.getLogger('mailer')

SMTP_SERVER = 'smtp.gmail.com'
PORT = 587


def connect():
    context = ssl.create_default_context()
    server = None
    try:
        server = smtplib.SMTP(SMTP_SERVER, PORT)
        server.starttls(context=context) # The context is to secure the connection
        server.login(email_config.sender_mail_id, email_config.sender_password)
    except Exception as e:
        logger.error(f'Error while connecting to smtp server. {e}')
        logger.error(traceback.format_exc())
    return server


def attach_file(message, files):
    files = [] if not files else [files] if not isinstance(files, list) else files
    for file in files:
        with open(file, "rb") as attachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', f"attachment; filename= {os.path.basename(file)}",)
        message.attach(part)


def send_mail_notification(payload, files=None):
    server = connect()
    for name, mail_id in email_config.users:
        message_object = MIMEMultipart()
        message_object['From'] = email_config.sender_mail_id
        message_object['To'] = mail_id
        message_object['Subject'] = 'TRAIN notification'
        message_object.attach(MIMEText(payload, 'plain'))
        attach_file(message_object, files)
        server.send_message(message_object)
        del message_object

    server.quit()


# if __name__ == '__main__':
#    send_mail_notification('test test', [r'C:\Users\Ashish\Downloads\COPYRIGHT.pdf'])
