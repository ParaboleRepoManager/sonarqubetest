import os
import sys
import logging
import logging.config
import traceback
import threading
import multiprocessing
import yaml
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler
from dateutil.relativedelta import relativedelta
from datetime import datetime


class LogFilter(logging.Filter):
    def filter(self, rec):
        if any(substring in rec.name for substring in ['kafka.', 'azure.', 'matplotlib', 'kazoo.', 'MonthlyInfoLogger']):
            return False
        return True


class CustomLogHandler(logging.Handler):
    def __init__(self, filename, max_bytes, backup_count):
        logging.Handler.__init__(self)
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        self._handler = RotatingFileHandler(filename, maxBytes=max_bytes, backupCount=backup_count, delay=True)
        self.queue = multiprocessing.Queue(-1)
        threading.Thread(target=self.receive, daemon=True).start()

    def setFormatter(self, fmt):
        logging.Handler.setFormatter(self, fmt)
        self._handler.setFormatter(fmt)

    def receive(self):
        while True:
            try:
                record = self.queue.get()
                self._handler.emit(record)
            except (KeyboardInterrupt, SystemExit):
                raise
            except EOFError:
                break
            except:
                traceback.print_exc(file=sys.stderr)

    def send(self, s):
        self.queue.put_nowait(s)

    def _format_record(self, record):
        if record.args:
            record.msg = record.msg % record.args
            record.args = None
        if record.exc_info:
            dummy = self.format(record)
            record.exc_info = None

        return record

    def emit(self, record):
        try:
            s = self._format_record(record)
            self.send(s)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def close(self):
        self._handler.close()
        logging.Handler.close(self)


class MonthlyInfoLogger(TimedRotatingFileHandler):
    def set_custom(self):
        self.suffix = datetime.now().strftime("%Y_%B")
        self.rolloverAt = int((datetime.now() + relativedelta(day=31, days=1)).replace(hour=0, minute=0, second=0, microsecond=0).timestamp())

    def __init__(self, filename, when='h', interval=1, backupCount=0, encoding=None, delay=False, utc=False, atTime=None):
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        TimedRotatingFileHandler.__init__(self, filename, when='h', interval=1, backupCount=0, encoding=None, delay=False, utc=False, atTime=None)
        self.set_name('MonthlyLogger')
        self.set_custom()

    def doRollover(self):
        TimedRotatingFileHandler.doRollover(self)
        self.set_custom()


logging.config.dictConfig(yaml.safe_load(open(os.path.join(os.path.dirname(__file__), 'logging.yaml')).read()))
info_logger = logging.getLogger('MonthlyInfoLogger')
