from azure.storage.blob import ContainerClient, generate_blob_sas, BlobSasPermissions, BlobProperties, BlobPrefix
from azure.core.exceptions import ResourceExistsError, ResourceNotFoundError, HttpResponseError
from datetime import datetime, timedelta
import storage_config as cfg
from manager_utils import retry
import logging
import logging_util
logger = logging.getLogger(__name__)


class Azure:
    @staticmethod
    def __get_container_service(config):
        if not config:
            return Azure.container

        connect_str = config['azure']['conn_string']
        container_name = config['azure']['container']
        container = ContainerClient.from_connection_string(conn_str=connect_str, container_name=container_name)
        try:
            container.create_container()
        except ResourceExistsError as error:
            pass
        return container

    @staticmethod
    def write(file_url, file_data, config, lease=None):
        Azure.__get_container_service(config).upload_blob(file_url, file_data, overwrite=True, max_concurrency=25, lease=lease)

    @staticmethod
    @retry(exception=HttpResponseError, tries=3)
    def read(file_url, config):
        return Azure.__get_container_service(config).download_blob(file_url, max_concurrency=25).readall()

    @staticmethod
    def move(src_url, dst_url, config):
        container = Azure.__get_container_service(config)
        src_blob = container.get_blob_client(src_url)
        container.get_blob_client(dst_url).start_copy_from_url(src_blob.url)
        src_blob.delete_blob()

    @staticmethod
    def file_exists(file_url, config):
        try:
            Azure.__get_container_service(config).get_blob_client(file_url).get_blob_properties()
        except ResourceNotFoundError as error:
            return False
        return True

    @staticmethod
    def get_external_path(blob_url, config):
        if not config: config = cfg.storage_cfg
        connect_str = config['azure']['conn_string']
        c = dict(s.split('=', 1) for s in connect_str.split(';') if s)
        token = generate_blob_sas(c['AccountName'], config['azure']['container'], blob_url, account_key=c['AccountKey'],
                                  permission=BlobSasPermissions(read=True), expiry=datetime.utcnow() + timedelta(hours=2))
        return f'{Azure.__get_container_service(config).get_blob_client(blob_url).url}?{token}'

    @staticmethod
    def get_files(dir_url, config, recursive=False):
        container = Azure.__get_container_service(config)
        dir_url = '' if dir_url == '/' else dir_url     # adjustment for 'root' folder

        func = container.list_blobs if recursive else container.walk_blobs
        blobs = func(name_starts_with=dir_url)
        files = [item.name[len(dir_url):].strip('/') for item in blobs if isinstance(item, BlobProperties)]
        return files

    @staticmethod
    def get_dirs(dir_url, config):
        container = Azure.__get_container_service(config)
        dir_url = '' if dir_url == '/' else dir_url     # adjustment for 'root' folder

        blobs = container.walk_blobs(name_starts_with=dir_url)
        dirs = [item.name[len(dir_url):].strip('/') for item in blobs if isinstance(item, BlobPrefix)]
        return dirs

    @staticmethod
    def clone_dir(src_dir, dst_dir, config):
        container = Azure.__get_container_service(config)
        src_dir = '' if src_dir == '/' else src_dir     # adjustment for 'root' folder

        for blob in container.list_blobs(name_starts_with=src_dir):
            src_blob = container.get_blob_client(blob.name)
            dst_blob = container.get_blob_client(dst_dir + blob.name)
            dst_blob.start_copy_from_url(src_blob.url)

    @staticmethod
    def delete_dir(dir_url, config):
        container = Azure.__get_container_service(config)
        dir_url = '' if dir_url == '/' else dir_url     # adjustment for 'root' folder
        container.delete_blobs(*container.list_blobs(name_starts_with=dir_url))

    @staticmethod
    @retry(exception=HttpResponseError, delay=0.1, tries=10)
    def lock_blob(file_url, config):
        return Azure.__get_container_service(config).get_blob_client(file_url).acquire_lease(lease_duration=15)

    @staticmethod
    def unlock_blob(lease):
        lease.release()

    if cfg.StorageEnum.AZURE in cfg.current_storage:
        container = __get_container_service.__func__(cfg.storage_cfg)
