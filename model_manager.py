import io
import os
import pandas as pd
import enum
from storage_utils import Storage
import storage_config as cfg
from zookeeper_utils import  Zookeeper
from enumerations import ModuleEnum, AttributeEnum
import logging
import logging_util

logger = logging.getLogger('model')
MODEL_INFO_FILE = 'model_info.csv'


class TypeEnum(enum.Enum):
    UNSUPERVISED = 'unsupervised'
    SUPERVISED = 'supervised'
    SUPERVISED_ENTITY = 'supervised_entity'
    OTHERS = 'others'


def update_model_info(module: ModuleEnum, type_: TypeEnum, url):
    model = Zookeeper.read(ModuleEnum.USER_INPUT, AttributeEnum.MODEL)
    iter_ = Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)
    url = os.path.normpath(os.path.join(model, f'iteration_{iter_}', url))

    if not Storage.file_exists(MODEL_INFO_FILE, from_base=True):
        Storage.write(MODEL_INFO_FILE, b'module,type,model,url', from_base=True)
    df = pd.read_csv(io.BytesIO(Storage.read(MODEL_INFO_FILE, from_base=True)), encoding='utf8')

    matches = (df['module'] == module.value) & (df['type'] == type_.value) & (df['model'] == model) & (df['url'] == url)
    row_count, _ = df[matches].shape
    if row_count == 0:
        next_index, _ = df.shape
        df.loc[next_index] = [module.value, type_.value, model, url]
    else:
        df.loc[matches, 'url'] = url

    text_stream = io.StringIO()
    df.to_csv(text_stream, index=False)
    Storage.write(MODEL_INFO_FILE, bytes(text_stream.getvalue(), encoding='utf-8'), from_base=True)


def get_model_path(domain, module: ModuleEnum, type_: TypeEnum, model=None):
    store = 'azure' if cfg.current_storage == cfg.StorageEnum.AZURE else \
        'amazon' if cfg.current_storage == cfg.StorageEnum.AWS else \
            'local' if cfg.current_storage == cfg.StorageEnum.LOCAL else \
                'network' if cfg.current_storage == cfg.StorageEnum.NETWORK else ''
    url = os.path.normpath(os.path.join(cfg.storage_cfg[store]["folder"], domain, MODEL_INFO_FILE))
    if Storage.file_exists(url, is_absolute=True):
        df = pd.read_csv(io.BytesIO(Storage.read(url, is_absolute=True)), encoding='utf8')
        matches = (df['module'] == module.value) & (df['type'] == type_.value)
        if model:
            matches = matches & (df['model'] == model)
        df = df[matches]
        op = {}
        if not df.empty:
            for i in range(len(df)):
                op[df.iloc[i].model] = Storage.get_external_path(df.iloc[i].url, from_base=True)
            return op
