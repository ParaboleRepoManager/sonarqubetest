from flask import jsonify
from enum import Enum
import traceback

import logging
# import logging_util
logger = logging.getLogger(__name__)


class ErrorCode(Enum):
    FatalException = 0
    NotFound = 50
    AlreadyExits = 51
    MissingMandatory = 52
    CustomException = 100


class CustomException(Exception):
    def __init__(self, code=ErrorCode.CustomException.value, message=None, description=None):
        self.code = code
        self.message = message
        self.description = description


class NotFound(CustomException):
    def __init__(self, message=None, description=None):
        super().__init__(ErrorCode.NotFound.value, message, description)


class AlreadyExits(CustomException):
    def __init__(self, message=None, description=None):
        super().__init__(ErrorCode.AlreadyExits.value, message, description)


class MissingMandatory(CustomException):
    def __init__(self, message=None, description=None):
        super().__init__(ErrorCode.MissingMandatory.value, message, description)


def set_error_handler(app):
    @app.errorhandler(Exception)
    def handle_exception(e):
        if isinstance(e, CustomException):
            return jsonify({'code': e.code, 'message': e.message, 'description': e.description}), 500

        logger.exception('Unhandled Exception:')
        return jsonify({'code': ErrorCode.FatalException.value, 'message': 'An unrecoverable error occured.', 'description': 'Please contact System Administrator of help'}), 500
