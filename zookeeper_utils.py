import datetime
import sys
import traceback
import json
import os
import time
import zk_config as cfg
import importlib
from kazoo.exceptions import BadVersionError
from kazoo.retry import ForceRetryError
from kazoo.client import KazooClient
from enumerations import *
from exceptions import AlreadyExits

import logging
loading = False
#import logging_util
logger = logging.getLogger(__name__)


class CallBack:
    watch = True
    def on_change(self, data, stat): logger.info(data)


class ZKLoadingCallback(CallBack):
    def on_change(self, data, stat):
        global loading
        loading = data


class Zookeeper:
    zk = None

    @staticmethod
    def refresh_instance():
        logging.info(f'PROCESS ID: {os.getpid()}')
        temp=KazooClient(hosts=cfg.zk_host_ip, read_only=False)
        Zookeeper.zk = temp
        Zookeeper.zk.start(timeout=cfg.zk_connect_timeout)
        logging.info(f'TEMP: {id(temp)}')

    @staticmethod
    def exists(module: ModuleEnum, attribute: AttributeEnum, root=None):
        node = cfg.zk_node_path_map.get(f'{module.value}_{attribute.value}')
        if isinstance(node, tuple):
            node, _ = node
        if root:
            p = node.split('/')
            p[1] = root
            node = '/'.join(p)
        return Zookeeper.zk.exists(node)

    @staticmethod
    def change_counter(module: ModuleEnum, attribute: AttributeEnum, value):
        def _inner_change():
            node = cfg.zk_node_path_map.get(f'{module.value}_{attribute.value}')
            if isinstance(node, tuple):
                node, _ = node
            data, stat = Zookeeper.zk.get(node)
            data = 0 if data == b'' else json.loads(str(data, 'utf-8'))
            version = stat.version
            data = data + value
            try:
                Zookeeper.zk.set(node, bytes(json.dumps(data), 'utf-8'), version)
                return data
            except BadVersionError:
                logger.warning(f'zookeeper_utils | Version Error | could not write | Retrying')
                raise ForceRetryError()
        return Zookeeper.zk.retry(_inner_change)

    @staticmethod
    def change_counter_to_limit(module: ModuleEnum, attribute: AttributeEnum, value, limit):
        def _inner_change():
            node = cfg.zk_node_path_map.get(f'{module.value}_{attribute.value}')
            data, stat = Zookeeper.zk.get(node)
            data = json.loads(str(data, 'utf-8'))
            if data + value > limit:
                return -1  # indicates value already reached to limit
            version = stat.version
            data = data + value
            if isinstance(node, tuple):
                node, _ = node
            try:
                Zookeeper.zk.set(node, bytes(json.dumps(data), 'utf-8'), version)
                return data
            except BadVersionError:
                logger.warning(f'zookeeper_utils | Version Error | could not write | Retrying')
                raise ForceRetryError()

        return Zookeeper.zk.retry(_inner_change)

    @staticmethod
    def write(module: ModuleEnum, attribute: AttributeEnum, value):
        node = cfg.zk_node_path_map.get(f'{module.value}_{attribute.value}')
        if isinstance(node, tuple):
            node, _ = node
        Zookeeper.zk.set(node, bytes(json.dumps(value), 'utf-8'))

    @staticmethod
    def read(module: ModuleEnum, attribute: AttributeEnum, root=None):
        node = cfg.zk_node_path_map.get(f'{module.value}_{attribute.value}')
        if isinstance(node, tuple):
            node, _ = node
        if root:
            p = node.split('/')
            p[1] = root
            node = '/'.join(p)
        data, stat = Zookeeper.zk.retry(Zookeeper.zk.get, node)
        if data is None:
            return {}
        return json.loads(str(data, 'utf-8'))

    @staticmethod
    def read_with_stat(module: ModuleEnum, attribute: AttributeEnum, root=None):
        node = cfg.zk_node_path_map.get(f'{module.value}_{attribute.value}')
        if isinstance(node, tuple):
            node, _ = node
        if root:
            p = node.split('/')
            p[1] = root
            node = '/'.join(p)
        data, stat = Zookeeper.zk.retry(Zookeeper.zk.get, node)
        if data is None:
            return {}
        return json.loads(str(data, 'utf-8')), stat.version

    @staticmethod
    def watch(module: ModuleEnum, attribute: AttributeEnum, callback: CallBack):
        node = cfg.zk_node_path_map.get(f'{module.value}_{attribute.value}')
        if isinstance(node, tuple):
            node, _ = node

        @Zookeeper.zk.DataWatch(node)
        def watch(data, stat, event):
            if event and callback.watch and not loading:
                callback.on_change(json.loads(str(data, 'utf-8')), stat)
            return callback.watch

    @staticmethod
    def unwatch(callback: CallBack): callback.watch = False

    @staticmethod
    def delete(node):
        node_path = cfg.zk_node_path_map.get(node)
        Zookeeper.zk.delete(node_path, recursive=True)

    @staticmethod
    def read_path(path, append_root=True):
        path = path.strip('/\\')
        if append_root:
            data, stat = Zookeeper.zk.retry(Zookeeper.zk.get, f'/{cfg.zk_root_path}/{path}')
        else:
            data, stat = Zookeeper.zk.retry(Zookeeper.zk.get, f'/{path}')
        if data is None:
            return {}
        return json.loads(str(data, 'utf-8'))

    @staticmethod
    def write_path(path, value, append_root=True):
        path = path.strip('/\\')
        if append_root:
            path = f'/{cfg.zk_root_path}/{path}'
        Zookeeper.zk.ensure_path(path)
        Zookeeper.zk.set(path, bytes(json.dumps(value), 'utf-8'))

    @staticmethod
    def reset_config():
        for node in cfg.zk_node_path_map.values():
            if isinstance(node, tuple):
                node,_ = node
            key = node
            if key.endswith('processed'):
                Zookeeper.zk.set(node, bytes(json.dumps(False), 'utf-8'))
            if key.endswith('running'):
                Zookeeper.zk.set(node, bytes(json.dumps(False), 'utf-8'))
            if key.endswith('_count'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), 'utf-8'))
            if key.endswith('_metadata'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), encoding='utf-8'))
            if key.endswith('_download_document'):
                Zookeeper.zk.set(node, bytes(json.dumps(False), encoding='utf-8'))
            if key.endswith('_iteration'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), encoding='utf-8'))
            if key.endswith('_received'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), encoding='utf-8'))
            if key.endswith('_size'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), encoding='utf-8'))
            if key.endswith('status'):
                Zookeeper.zk.set(node, bytes(json.dumps('awaiting'), 'utf-8'))
            if key.endswith('failed'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), encoding='utf-8'))
            if key.endswith('selected'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), encoding='utf-8'))
            if key.endswith('rejected'):
                Zookeeper.zk.set(node, bytes(json.dumps(0), encoding='utf-8'))

    @staticmethod
    def set_build_complete():
        Zookeeper.write_path(f'{cfg.zk_deployment_path}/_build_active', False, append_root=False)

    @staticmethod
    def get_build_info():
        org = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_org', False)
        domain = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_domain', False)
        sub_domain = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_sub_domain', False)
        build_name = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_build_name', False)
        build_type = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_build_type', False)
        return org, domain, sub_domain, build_name, build_type

    @staticmethod
    def get_build_path(org=None, domain=None, sub_domain=None, build_name=None):
        org = org if org else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_org', False)
        domain = domain if domain else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_domain', False)
        sub_domain = sub_domain if sub_domain else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_sub_domain', False)
        build_name = build_name if build_name else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_build_name', False)
        return f'{cfg.zk_deployment_path}/{org}/{domain}/{sub_domain}/{build_name}'

    @staticmethod
    def get_build_prefix(org=None, domain=None, sub_domain=None, build_name=None, build_type=None):
        org = org if org else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_org', False)
        domain = domain if domain else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_domain', False)
        sub_domain = sub_domain if sub_domain else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_sub_domain', False)
        build_name = build_name if build_name else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_build_name', False)
        build_type = build_type if build_type else Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_build_type', False)
        return f'{cfg.zk_deployment_path}/{org}/{domain}/{sub_domain}/{build_name}/{build_type}'

    @staticmethod
    def create_build_nodes(org, domain, sub_domain, build_name, build_type, env):
        def check_active_build():
            if Zookeeper.zk.exists(f'/{cfg.zk_deployment_path}/_build_active'):
                return Zookeeper.read_path(f'{cfg.zk_deployment_path}/_build_active', append_root=False)
            return False

        def set_root_nodes():
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_env', f'{env}', append_root=False)
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_org', f'{org}', append_root=False)
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_domain', f'{domain}', append_root=False)
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_sub_domain', f'{sub_domain}', append_root=False)
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_build_name', f'{build_name}', append_root=False)
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_build_type', build_type, append_root=False)
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_build_id', 0, append_root=False)
            Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_model_id', 0, append_root=False)
            #Zookeeper.write_path(f'/{cfg.zk_deployment_path}/_build_active', 0, append_root=False)
            Zookeeper.write_path(f'{cfg.zk_deployment_path}/{org}/{domain}/{sub_domain}/{build_name}', time.time(), False)
            Zookeeper.zk.ensure_path(f'{cfg.zk_deployment_path}/{org}/{domain}/{sub_domain}/{build_name}/unsupervised')
            Zookeeper.zk.ensure_path(f'{cfg.zk_deployment_path}/{org}/{domain}/{sub_domain}/{build_name}/supervised')
            Zookeeper.zk.ensure_path(f'{cfg.zk_deployment_path}/{org}/{domain}/{sub_domain}/{build_name}/analysis')

        def set_module_nodes():
            env = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_env', append_root=False)
            iter_path = f'/{Zookeeper.get_build_prefix()}/{ModuleEnum.CONFIG.value}/{AttributeEnum.LEARNING_ITERATION.value}'
            has_iter = Zookeeper.zk.exists(iter_path)

            for key, path in cfg.zk_node_path_map.items():
                if isinstance(path, tuple):
                    path, value = path
                    path = path.format(zk_root_path=cfg.zk_root_path, env=env)
                    if isinstance(value, str):
                        value = value.format(zk_root_path=cfg.zk_root_path, env=env)
                    if path != iter_path or not has_iter:
                        Zookeeper.write_path(path, value, append_root=False)
                    cfg.zk_node_path_map[key] = (path, value)
                else:
                    path = path.format(zk_root_path=cfg.zk_root_path, env=env)
                    Zookeeper.write_path(path, None, append_root=False)
                    cfg.zk_node_path_map[key] = path

        if check_active_build():
            raise AlreadyExits('Exists Already', 'An existing build is already in progress!')
        set_root_nodes()
        importlib.reload(cfg)
        cfg.zk_root_path = Zookeeper.get_build_prefix()
        set_module_nodes()

    try:
        logger.info(f'Trying to connect to zookeeper server. Contacting {cfg.zk_host_ip} . . .')
        zk = KazooClient(hosts=cfg.zk_host_ip, read_only=False)
        zk.start(timeout=cfg.zk_connect_timeout)
    except ConnectionError:
        logger.error(f'Error connecting to zookeeper. {sys.exc_info()[0]}. Complete stacktrace: '
                     f'{traceback.format_exc()}', exc_info=True)
        raise


    @staticmethod
    def init_const_zk_nodes():
        for key, path in cfg.zk_node_path_map.items():
            if '{{zk_root_path}}' in path:
                continue
            if isinstance(path, tuple):
                path, value = path
                Zookeeper.write_path(path, value, append_root=False)
            else:
                Zookeeper.write_path(path, None, append_root=False)

    @staticmethod
    def reload_cfg():
        importlib.reload(cfg)
        cfg.zk_root_path = Zookeeper.get_build_prefix()
        env = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_env', append_root=False)
        for key, path in cfg.zk_node_path_map.items():
            if isinstance(path, tuple):
                path, value = path
                path = path.format(zk_root_path=cfg.zk_root_path, env=env)
                if isinstance(value, str):
                    value = value.format(zk_root_path=cfg.zk_root_path, env=env)
                cfg.zk_node_path_map[key] = (path, value)
            else:
                path = path.format(zk_root_path=cfg.zk_root_path, env=env)
                cfg.zk_node_path_map[key] = path

@Zookeeper.zk.DataWatch(f'/{cfg.zk_deployment_path}/_build_active')
def watch(data, stat, event=None):
    def set_config_paths():
        env = Zookeeper.read_path(f'/{cfg.zk_deployment_path}/_env', append_root=False)
        for key, path in cfg.zk_node_path_map.items():
            if isinstance(path, tuple):
                path, value = path
                path = path.format(zk_root_path=cfg.zk_root_path, env=env)
                if isinstance(value, str):
                    value = value.format(zk_root_path=cfg.zk_root_path, env=env)
                cfg.zk_node_path_map[key] = (path, value)
            else:
                path = path.format(zk_root_path=cfg.zk_root_path, env=env)
                cfg.zk_node_path_map[key] = path

    if event and event.type == 'CHANGED' and json.loads(str(data, 'utf-8')):
        importlib.reload(cfg)
        cfg.zk_root_path = Zookeeper.get_build_prefix()
        set_config_paths()

        main_module = sys.modules['__main__']
        if 'new_build_active' in dir(main_module):
            # Zookeeper.zk.restart()
            main_module.new_build_active()


#Zookeeper.zk.delete('{zk_root_path}', recursive=True)
#Zookeeper.zk.delete('testnaming', recursive=True)
#Zookeeper.zk.set('/testnaming/_build_active', bytes(json.dumps(False), 'utf-8'))
#Zookeeper.create_build_nodes('Google_1', 'search', 'search_sub', 'my_build_1', 'supervised')
