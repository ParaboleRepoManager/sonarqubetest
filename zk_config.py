zk_host_ip = '127.0.0.1:2181'
zk_connect_timeout = 10
zk_deployment_path = 'testnaming'  # put this unique for each deployment
zk_root_path = None

zk_node_path_map = {
    'config_learning_type': (f'/{{zk_root_path}}/config/learning_type', 'unsupervised'),
    'config_download': (f'/{{zk_root_path}}/config/download', True),
    'config_phase': (f'/{{zk_root_path}}/config/phase', 'corpus_generation'),
    'config_golden_documents_type': (f'/{{zk_root_path}}/config/golden_documents_type', 'raw'),
    'config_contexts': (f'/{{zk_root_path}}/config/contexts', []),
    'config_dir_golden_terms': (f'/{{zk_root_path}}/config/golden_terms', 'config/golden_terms'),
    'config_dir_corpus_documents': (f'/{{zk_root_path}}/config/corpus/documents', 'config/corpus/documents'),

    'starter_status': (f'/{{zk_root_path}}/starter/status', 'awaiting'),
    'starter_definition_document_count': (f'/{{zk_root_path}}/starter/definition_document_count', 0),

    'starter_golden_document_count': (f'/{{zk_root_path}}/starter/golden_document_count', 0),
    'starter_seed_document_count': (f'/{{zk_root_path}}/starter/seed_document_count', 0),
    'starter_context_def_count': (f'/{{zk_root_path}}/starter/context_def_count', 0),
    'starter_golden_document_processed': f'/{{zk_root_path}}/starter/golden_document_processed',
    'starter_seed_document_processed': f'/{{zk_root_path}}/starter/seed_document_processed',
    'starter_term_processed': f'/{{zk_root_path}}/starter/term_processed',
    'starter_defs_processed': f'/{{zk_root_path}}/starter/defs_processed',
    'starter_feedback_processed': f'/{{zk_root_path}}/starter/feedback_processed',

    'controller': f'/{{zk_root_path}}/controller',
    'controller_status': f'/{{zk_root_path}}/controller/status',
    'controller_term_processed': f'/{{zk_root_path}}/controller/term_processed',
    'controller_document_processed': f'/{{zk_root_path}}/controller/document_processed',
    'controller_document_count': (f'/{{zk_root_path}}/controller/document_count', 0),
    'controller_seed_document_processed': f'/{{zk_root_path}}/controller/seed_document_processed',
    'controller_seed_document_count': (f'/{{zk_root_path}}/controller/seed_document_count', 0),
    'controller_golden_document_processed': f'/{{zk_root_path}}/controller/golden_document_processed',
    'controller_golden_document_count': (f'/{{zk_root_path}}/controller/golden_document_count', 0),
    'controller_data_list': (f'/{{zk_root_path}}/controller/data_list', []),

    # todo: verify below items and bring them above this line ---------------------------------------------------------


    'config_domain': (f'/{{zk_root_path}}/config/domain', f'{zk_deployment_path}'),
    'config_sentence_ranker_auto': (f'/{{zk_root_path}}/config/sentence_ranker_auto', 'false'),
    'config_golden_terms_loaded': (f'/{{zk_root_path}}/config/golden_terms_loaded', 'False'),
    'config_unique_file_no': (f'/{{zk_root_path}}/config/unique_file_no', 0),
    'config_learning_iteration': (f'/{{zk_root_path}}/config/learning_iteration', 0),
    'config_corpus_iteration': f'/{{zk_root_path}}/config/corpus_iteration',
    'config_corpus_generation_iteration': f'/{{zk_root_path}}/config/corpus_generation_iteration',
    'config_zk_loading': (f'/{{zk_root_path}}/config/zk_loading', False),
    'config_document_count': (f'/{{zk_root_path}}/config/document_count', 0),
    'config_golden_url': f'/{{zk_root_path}}/config/golden_url',

    'auto_ontology_message_count': (f'/{{zk_root_path}}/auto_ontology/message_count', 0),
    'auto_ontology_message_received': (f'/{{zk_root_path}}/auto_ontology/message_received', 0),
    'auto_ontology_status': (f'/{{zk_root_path}}/auto_ontology/status', 'awaiting'),
    'auto_ontology_term_cluster': f'/{{zk_root_path}}/auto_ontology/term_cluster',
    'auto_ontology_doc_level_processed': f'/{{zk_root_path}}/auto_ontology/doc_level_processed',
    'auto_ontology_defs_processed': f'/{{zk_root_path}}/auto_ontology/defs_processed',
    'auto_ontology_term_processed': f'/{{zk_root_path}}/auto_ontology/term_processed',
    'auto_ontology_definition_document_count': (f'/{{zk_root_path}}/auto_ontology/definition_document_count', 0),
    'auto_ontology_term_message_count': (f'/{{zk_root_path}}/auto_ontology/term_message_count', 0),
    'auto_ontology_output_path': f'/{{zk_root_path}}/auto_ontology/output_path',
    'auto_ontology_corenlp_receive_count': (f'/{{zk_root_path}}/auto_ontology/corenlp_receive_count', 0),
    'auto_ontology_failed': f'/{{zk_root_path}}/auto_ontology/failed',
    'auto_ontology_phase_doc_count': (f'/{{zk_root_path}}/auto_ontology/phase_doc_count', 0),
    'auto_ontology_ao_phase_stats': (f'/{{zk_root_path}}/auto_ontology/ao_phase_stats', {}),
    'auto_ontology_ao_stats': (f'/{{zk_root_path}}/auto_ontology/ao_stats', {}),
    'auto_ontology_output_val': f'/{{zk_root_path}}/auto_ontology/output_val',
    'auto_ontology_input_val': f'/{{zk_root_path}}/auto_ontology/input_val',
    'auto_ontology_term_count': (f'/{{zk_root_path}}/auto_ontology/term_count', 0),

    'core_nlp': f'/{{zk_root_path}}/core_nlp',
    'core_nlp_message_count': (f'/{{zk_root_path}}/core_nlp/message_count', 0),
    'core_nlp_message_received': (f'/{{zk_root_path}}/core_nlp/message_received', 0),
    'core_nlp_status': (f'/{{zk_root_path}}/core_nlp/status', 'awaiting'),
    'core_nlp_data_file_path': f'/{{zk_root_path}}/core_nlp/data_file_path',
    'core_nlp_message_arrived': f'/{{zk_root_path}}/core_nlp/message_arrived',
    'core_nlp_file_name_list': f'/{{zk_root_path}}/core_nlp/file_name_list',
    'core_nlp_failed': f'/{{zk_root_path}}/core_nlp/failed',
    'core_nlp_triples_count': (f'{{zk_root_path}}/core_nlp/triples_count', 0),
    'core_nlp_sentence_count': (f'{{zk_root_path}}/core_nlp/sentence_count', 0),

    'requirement_extractor_processed': f'/{{zk_root_path}}/req_extr/processed',

    'table_extractor_processed': f'/{{zk_root_path}}/tab_extr/processed',

    'rdf_creator_processed': f'/{{zk_root_path}}/rdf_creator/processed',

    'corpus_filter': f'/{{zk_root_path}}/corpus_filter',
    'corpus_filter_status': (f'/{{zk_root_path}}/corpus_filter/status', 'awaiting'),
    'corpus_filter_running': f'/{{zk_root_path}}/corpus_filter/running',
    'corpus_filter_metadata': f'/{{zk_root_path}}/corpus_filter/metadata',
    'corpus_filter_contexts': f'/{{zk_root_path}}/corpus_filter/contexts',
    'corpus_filter_well_represented_contexts': f'/{{zk_root_path}}/document_filter/well_represented_contexts',

    'document_filter': f'/{{zk_root_path}}/document_filter',
    'document_filter_message_count': (f'/{{zk_root_path}}/document_filter/message_count', 0),
    'document_filter_message_received': (f'/{{zk_root_path}}/document_filter/message_received',0),
    'document_filter_status': (f'/{{zk_root_path}}/document_filter/status', 'awaiting'),
    'document_filter_running': f'/{{zk_root_path}}/document_filter/running',
    'document_filter_failed': f'/{{zk_root_path}}/document_filter/failed',
    'document_filter_term_count': (f'/{{zk_root_path}}/document_filter/term_count', 0),
    'document_filter_rejected': f'/{{zk_root_path}}/document_filter/rejected',
    'document_filter_selected': f'/{{zk_root_path}}/document_filter/selected',
    'document_filter_threshold': (f'/{{zk_root_path}}/document_filter/threshold', 0),

    'document_reader_storage': f'/{{zk_root_path}}/document_reader_storage',
    'document_reader_storage_message_count': (f'/{{zk_root_path}}/document_reader_storage/message_count', 0),
    'document_reader_storage_message_received': (f'/{{zk_root_path}}/document_reader_storage/message_received', 0),
    'document_reader_storage_status': (f'/{{zk_root_path}}/document_reader_storage/status', 'awaiting'),

    'document_reader_downloader': f'/{{zk_root_path}}/document_reader_downloader',
    'document_reader_downloader_message_count': (f'/{{zk_root_path}}/document_reader_downloader/message_count', 0),
    'document_reader_downloader_message_received': (f'/{{zk_root_path}}/document_reader_downloader/message_received', 0),
    'document_reader_downloader_status': (f'/{{zk_root_path}}/document_reader_downloader/status', 'awaiting'),
    'document_reader_downloader_failed': f'/{{zk_root_path}}/document_reader_downloader/failed',

    'downloader': f'/{{zk_root_path}}/downloader',
    'downloader_message_count': (f'/{{zk_root_path}}/downloader/message_count', 0),
    'downloader_download_count': (f'/{{zk_root_path}}/downloader/download_count', 0),
    'downloader_status': (f'/{{zk_root_path}}/downloader/status', 'awaiting'),
    'downloader_golden_download_count': (f'/{{zk_root_path}}/downloader/golden_download_count', 0),
    'downloader_context_download_count': (f'/{{zk_root_path}}/downloader/context_download_count', 0),
    'downloader_golden_download_size': f'/{{zk_root_path}}/downloader/golden_download_size',
    'downloader_context_download_size': f'/{{zk_root_path}}/downloader/context_download_size',

    'corpus_preprocessor': f'/{{zk_root_path}}/corpus_preprocessor',

    'bert_unsupervised': f'/{{zk_root_path}}/bert_unsupervised',

    'bert_supervised': f'/{{zk_root_path}}/bert_supervised',
    'bert_supervised_running': (f'/{{zk_root_path}}/bert_supervised/running', False),
    'bert_supervised_status': (f'/{{zk_root_path}}/bert_supervised/status', 'awaiting'),
    'bert_supervised_output': (f'/{{zk_root_path}}/bert_supervised/output', 'default'),
    'bert_supervised_status': (f'/{{zk_root_path}}/bert_supervised/status', {}),
    'bert_supervised_input_file': (f'/{{zk_root_path}}/bert_supervised/input_file', f'{zk_deployment_path}_bert_supervised_input_file'),
    'bert_supervised_evaluation_result': (f'/{{zk_root_path}}/bert_supervised/evaluation_result', {}),
    'bert_supervised_feedback_arrived': (f'/{{zk_root_path}}/bert_supervised/feedback_arrived', False),

    'fasttext_unsupervised': f'/{{zk_root_path}}/fasttext_unsupervised',
    'fasttext_unsupervised_status': (f'/{{zk_root_path}}/fasttext_unsupervised/status', 'awaiting'),
    'fasttext_unsupervised_running': f'/{{zk_root_path}}/fasttext_unsupervised/running',
    'fasttext_unsupervised_message_count': (f'/{{zk_root_path}}/fasttext_unsupervised/message_count', 0),
    'fasttext_unsupervised_model_storage': f'/{{zk_root_path}}/fasttext_unsupervised/model_storage',
    'fasttext_unsupervised_status': (f'/{{zk_root_path}}/fasttext_unsupervised/status', {}),

    'fasttext_supervised': f'/{{zk_root_path}}/fasttext_supervised',
    'fasttext_supervised_running': (f'/{{zk_root_path}}/fasttext_supervised/running', False),
    'fasttext_supervised_output': (f'/{{zk_root_path}}/fasttext_supervised/output', 'default'),
    'fasttext_supervised_input_q': (f'/{{zk_root_path}}/fasttext_supervised/input_q', f'{zk_deployment_path}_train_supervised'),
    'fasttext_supervised_status': (f'/{{zk_root_path}}/fasttext_supervised/status', 'awaiting'),

    'fasttext_validator': f'/{{zk_root_path}}/fasttext_validator',
    'fasttext_validator_message_count': (f'/{{zk_root_path}}/fasttext_validator/message_count', 0),
    'fasttext_validator_status': (f'/{{zk_root_path}}/fasttext_validator/status', 'awaiting'),
    'fasttext_validator_running': f'/{{zk_root_path}}/fasttext_validator/running',

    'wordspace_unsupervised': f'/{{zk_root_path}}/wordspace_unsupervised',
    'wordspace_unsupervised_status': (f'/{{zk_root_path}}/wordspace_unsupervised/status', 'awaiting'),
    'wordspace_unsupervised_running': f'/{{zk_root_path}}/wordspace_unsupervised/running',
    'wordspace_unsupervised_message_count': (f'/{{zk_root_path}}/wordspace_unsupervised/message_count', 0),
    'wordspace_unsupervised_model_storage': f'/{{zk_root_path}}/wordspace_unsupervised/model_storage',
    'wordspace_unsupervised_status': (f'/{{zk_root_path}}/wordspace_unsupervised/status', {}),

    'wordspace_validator': f'/{{zk_root_path}}/wordspace_validator',
    'wordspace_validator_message_count': (f'/{{zk_root_path}}/wordspace_validator/message_count', 0),
    'wordspace_validator_status': (f'/{{zk_root_path}}/wordspace_validator/status', 'awaiting'),
    'wordspace_validator_running': f'/{{zk_root_path}}/wordspace_validator/running',

    'validation_aggregator': f'/{{zk_root_path}}/validation_aggregator',
    'validation_aggregator_message_count': (f'/{{zk_root_path}}/validation_aggregator/', 0),
    'validation_aggregator_status': (f'/{{zk_root_path}}/validation_aggregator/status', 'awaiting'),
    'validation_aggregator_metadata': f'/{{zk_root_path}}/validation_aggregator/metadata',

    'structure_extractor': f'/{{zk_root_path}}/structure_extractor',
    'structure_extractor_golden_documents': f'/{{zk_root_path}}/structure_extractor/golden_documents',
    'structure_extractor_message_count': (f'/{{zk_root_path}}/structure_extractor/message_count', 0),
    'structure_extractor_message_received': (f'/{{zk_root_path}}/structure_extractor/message_received', 0),
    'structure_extractor_status': (f'/{{zk_root_path}}/structure_extractor/status', 'awaiting'),
    'structure_extractor_content_msg_count': (f'{{zk_root_path}}/structure_extractor/content_msg_count', 0),
    'structure_extractor_failed': f'/{{zk_root_path}}/structure_extractor/failed',

    'sentence_ranker': f'/{{zk_root_path}}/sentence_ranker',
    'sentence_ranker_message_count': (f'/{{zk_root_path}}/sentence_ranker/message_count', 0),
    'sentence_ranker_status': (f'/{{zk_root_path}}/sentence_ranker/status', 'awaiting'),
    'sentence_ranker_running': f'/{{zk_root_path}}/sentence_ranker/running',
    'sentence_ranker_has_sentences': f'/{{zk_root_path}}/sentence_ranker/has_sentences',
    'sentence_ranker_others_output':(f'/{{zk_root_path}}/sentence_ranker/others_output', f'{zk_deployment_path}_sentence_ranker_others_output'),
    'sentence_ranker_training_set':(f'/{{zk_root_path}}/sentence_ranker/training_set', f'{zk_deployment_path}_sentence_ranker_training_set'),

    'user_input_model': f'/{{zk_root_path}}/user_input/model',
    'user_input_domain': (f'/{{zk_root_path}}/user_input/domain', 'DEFAULT'),
    'user_input_subdomain': (f'/{{zk_root_path}}/user_input/subdomain', f'{zk_deployment_path}'),
    'user_input_dimension': f'/{{zk_root_path}}/user_input/dimension',
    'user_input_namespace': f'/{{zk_root_path}}/user_input/namespace',
    'user_input_learning_type': f'/{{zk_root_path}}/user_input/learning_type',
    'user_input_environment': (f'/{{zk_root_path}}/user_input/environment', 'stage'),

    'doc_user_review_status': (f'/{{zk_root_path}}/doc_user_review/status', 'awaiting'),
    'term_user_review_status': (f'/{{zk_root_path}}/term_user_review/status', 'awaiting'),
    'doc_user_review_review_data': (f'/{{zk_root_path}}/doc_user_review/review_data', 'user_review/golden_links.json'),
    'term_user_review_term_count': (f'/{{zk_root_path}}/term_user_review/term_count', 0),

    'ontology_curator': f'/{{zk_root_path}}/ontology_curator',
    'ontology_curator_status': (f'/{{zk_root_path}}/ontology_curator/status', 'awaiting'),

    # Constant values are listed below
    'config_dir_context_terms': (f'/{zk_deployment_path}/constants/config/context_terms', 'config/context_terms'),
    'config_dir_context_defs': (f'/{zk_deployment_path}/constants/config/context_defs', 'config/context_defs'),
    'config_dir_corpus_parsed_files': (f'/{zk_deployment_path}/constants/config/corpus/parsed_files', 'config/corpus/parsed_files'),
    'config_dir_corpus_parsed_files_extractor': (f'/{zk_deployment_path}/constants/config/corpus/parsed_files_extractor', 'config/corpus/parsed_files_extractor'),
    'config_dir_corpus_parsed_jsons': (f'/{zk_deployment_path}/constants/config/corpus/parsed_jsons', 'config/corpus/parsed_jsons'),
    'config_dir_downloaded_documents': (f'/{zk_deployment_path}/constants/downloader/documents', 'downloader/documents'),

    'config_dir_term_pool': (f'/{zk_deployment_path}/constants/downloader/term_pool', 'downloader/term_pool'),

    'user_input_feedback_data': (f'/{zk_deployment_path}/constants/user_input/feedback_data', 'user_input/feedback_data'),
    # 'user_input_review_data': (f'/{zk_deployment_path}/constants/user_input/review_input', 'user_input/review_data'),
    'user_input_entity_input': (f'/{zk_deployment_path}/constants/user_input/entity_input', 'user_input/entity_input'),
    'user_input_golden_document_list': (f'/{zk_deployment_path}/constants/user_input/golden_document_list', 'user_input/golden_document_list'),


    # Kafka queues are listed below
    'starter_output_q_definitions': (f'/{zk_deployment_path}/queues/starter/output_q_definitions', f'{zk_deployment_path}_corpus'),
    'starter_output_q_docs_review': (f'/{zk_deployment_path}/queues/starter/output_q_docs_review', f'{zk_deployment_path}_docs_review'),
    'starter_output_q_terms_review': (f'/{zk_deployment_path}/queues/starter/output_q_terms_review', f'{zk_deployment_path}_terms_review'),
    'starter_output_q_train_supervised': (f'/{zk_deployment_path}/queues/starter/output_q_train_supervised', f'{zk_deployment_path}_train_supervised'),

    'term_filter_input_q_corpusF': (f'/{zk_deployment_path}/queues/term_filter/input_q_corpusF', f'{zk_deployment_path}_retrigger_download'),
    'controller_output_q': (f'/{zk_deployment_path}/queues/controller/output_q', f'{zk_deployment_path}_controller'),

    'auto_ontology_input_q': (f'/{zk_deployment_path}/queues/auto_ontology/input_q', f'{zk_deployment_path}_conll_ontology'),
    'auto_ontology_output_q': (f'/{zk_deployment_path}/queues/auto_ontology/auto_ontology_output_q', f'{zk_deployment_path}_ontology'),
    'auto_ontology_output_q_doc_level': (f'/{zk_deployment_path}/queues/auto_ontology/auto_ontology_output_q_doc_level', f'{zk_deployment_path}_doc_level'),

    'core_nlp_input_q': (f'/{zk_deployment_path}/queues/core_nlp/input_q', f'{zk_deployment_path}_vocab_analysis'),
    'core_nlp_input_q_corpus': (f'/{zk_deployment_path}/queues/core_nlp/input_q_corpus', f'{zk_deployment_path}_conll_internal'),
    'core_nlp_output_q': (f'/{zk_deployment_path}/queues/core_nlp/output_q', f'{zk_deployment_path}_conll_ontology'),

    'corpus_filter_input_q': (f'/{zk_deployment_path}/queues/corpus_filter/input_q', f'{zk_deployment_path}_corpus_analysis'),
    'corpus_filter_output_q': (f'/{zk_deployment_path}/queues/corpus_filter/output_q', f'{zk_deployment_path}_retrigger_download'),
    'corpus_filter_output_q_coreNLP': (f'/{zk_deployment_path}/queues/corpus_filter/output_q_coreNLP', f'{zk_deployment_path}_vocab_analysis'),

    'document_filter_input_q': (f'/{zk_deployment_path}/queues/document_filter/input_q', f'{zk_deployment_path}_doc_analysis'),
    'document_filter_output_q': (f'/{zk_deployment_path}/queues/document_filter/output_q', f'{zk_deployment_path}_filtered_docs'),

    'document_reader_storage_input_q': (f'/{zk_deployment_path}/queues/document_reader_storage/input_q', f'{zk_deployment_path}_corpus'),
    'document_reader_storage_output_q': (f'/{zk_deployment_path}/queues/document_reader_storage/output_q', f'{zk_deployment_path}_vocab_analysis'),

    'document_reader_downloader_input_q': (f'/{zk_deployment_path}/queues/document_reader_downloader/input_q', f'{zk_deployment_path}_download_dump'),
    'document_reader_downloader_output_q': (f'/{zk_deployment_path}/queues/document_reader_downloader/output_q', f'{zk_deployment_path}_doc_analysis'),

    'downloader_input_q': (f'/{zk_deployment_path}/queues/downloader/input_q', f'{zk_deployment_path}_download'),
    'downloader_output_q': (f'/{zk_deployment_path}/queues/downloader/output_q', f'{zk_deployment_path}_download_dump'),

    'corpus_preprocessor_input_q': (f'/{zk_deployment_path}/queues/corpus_preprocessor/input_q', f'{zk_deployment_path}_corpus_preprocessor'),
    'corpus_preprocessor_output_q': (f'/{zk_deployment_path}/queues/corpus_preprocessor/output_q', f'{zk_deployment_path}_corpus_processed'),

    'bert_unsupervised_input_q': (f'/{zk_deployment_path}/queues/bert_unsupervised/input_q', f'{zk_deployment_path}_bert_unsupervised'),

    'bert_supervised_input_q': (f'/{zk_deployment_path}/queues/bert_supervised/input_q', f'{zk_deployment_path}_train_supervised'),
    'bert_supervised_output_q': (f'/{zk_deployment_path}/queues/bert_supervised/output_q', f'{zk_deployment_path}_bert_supervised'),

    'fasttext_unsupervised_input_q': (f'/{zk_deployment_path}/queues/fasttext_unsupervised/input_q', f'{zk_deployment_path}_ontology'),
    'fasttext_unsupervised_output_q': (f'/{zk_deployment_path}/queues/fasttext_unsupervised/output_q', f'{zk_deployment_path}_fasttext'),

    'fasttext_validator_input_q': (f'/{zk_deployment_path}/queues/fasttext_validator/input_q', f'{zk_deployment_path}_fasttext'),
    'fasttext_validator_output_q': (f'/{zk_deployment_path}/queues/fasttext_validator/output_q', f'{zk_deployment_path}_fasttext_validation'),

    'wordspace_unsupervised_input_q': (f'/{zk_deployment_path}/queues/wordspace_unsupervised/input_q', f'{zk_deployment_path}_ontology'),
    'wordspace_unsupervised_output_q': (f'/{zk_deployment_path}/queues/wordspace_unsupervised/output_q', f'{zk_deployment_path}_wordspace'),

    'wordspace_validator_input_q': (f'/{zk_deployment_path}/queues/wordspace_validator/input_q', f'{zk_deployment_path}_wordspace'),
    'wordspace_validator_output_q': (f'/{zk_deployment_path}/queues/wordspace_validator/output_q', f'{zk_deployment_path}_wordspace_validation'),

    'validation_aggregator_input_q_wordspace': (f'/{zk_deployment_path}/queues/validation_aggregator/input_q_wordspace', f'{zk_deployment_path}_wordspace_validation'),
    'validation_aggregator_input_q_fasttext': (f'/{zk_deployment_path}/queues/validation_aggregator/input_q_fasttext', f'{zk_deployment_path}_fasttext_validation'),
    'validation_aggregator_input_q_fasttext_supervised': (f'/{zk_deployment_path}/queues/validation_aggregator/input_q_fasttext_supervised', f'{zk_deployment_path}_fasttext_s_validation'),
    'validation_aggregator_input_q_bert_supervised': (f'/{zk_deployment_path}/queues/validation_aggregator/input_q_bert_supervised', f'{zk_deployment_path}_bert_s_validation'),

    'structure_extractor_input_q': (f'/{zk_deployment_path}/queues/structure_extractor/input_q', f'{zk_deployment_path}_extractor_in'),

    'sentence_ranker_output_q': (f'/{zk_deployment_path}/queues/sentence_ranker/output_q', f'{zk_deployment_path}_sentence_rank'),
    'sentence_ranker_others_input': (f'/{zk_deployment_path}/queues/sentence_ranker/others_input', f'{zk_deployment_path}_sentence_ranker_others_input'),
    'sentence_ranker_prediction_set_input': (f'/{zk_deployment_path}/queues/sentence_ranker/prediction_set_input', f'{zk_deployment_path}_sentence_ranker_prediction_set_input'),
    'sentence_ranker_prediction_set_output': (f'/{zk_deployment_path}/queues/sentence_ranker/prediction_set_output', f'{zk_deployment_path}_sentence_ranker_prediction_set_output'),

    'user_review_output_q': (f'/{zk_deployment_path}/queues/user_review_output_q', f'{zk_deployment_path}_reviewed_data'),

    'analysis_input_q': (f'/{zk_deployment_path}/queues/analysis/input_q', f'{zk_deployment_path}_analysis_input'),
    'analysis_output_q': (f'/{zk_deployment_path}/queues/analysis/output_q', f'{zk_deployment_path}_analysis_output'),
}

