import os
import json
import storage_config as cfg
from ustorage_utils import Storage
from .zookeeper_utils import Zookeeper
import zk_config
from enumerations import ModuleEnum, AttributeEnum


class Naming:
    file_dict = {}

    @staticmethod
    def write():
        path = os.path.join(Storage.get_storage_prefix(), cfg.map_file_names)
        Storage.write(path, bytes(json.dumps(Naming.file_dict, default=str), 'utf-8'), is_absolute=True)

    @staticmethod
    def custom_name(filename):
        if filename in Naming.file_dict:
            return Naming.file_dict[filename]

        build_name = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_build_name', False)
        iter_ = Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)
        unique_no = Zookeeper.change_counter(ModuleEnum.CONFIG, AttributeEnum.UNIQUE_FILE_NO, 1)
        unique_name = f'{build_name}_{iter_}_{unique_no}'

        _, ext = os.path.splitext(filename)
        unique_name = unique_name + ext if len(ext) < 10 else unique_name
        Naming.file_dict[filename] = unique_name
        return unique_name

    @staticmethod
    def get_original_filenames(unique_name):
        for key, value in Naming.file_dict.items():
            if unique_name == value:
                return key
        return ''

    @staticmethod
    def initialize():
        try:
            path = os.path.join(Storage.get_storage_prefix(), cfg.map_file_names)
            if Storage.file_exists(path, is_absolute=True):
                Naming.file_dict = json.loads(Storage.read(path, is_absolute=True))
            else:
                Naming.file_dict = {}
        except:
            pass

