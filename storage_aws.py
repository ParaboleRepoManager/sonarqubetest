import boto3
import storage_config as cfg
import logging
# import logging_util
logger = logging.getLogger(__name__)


class AWS:
    @staticmethod
    def __get_bucket_client(config):
        bucket = config['amazon']['bucket'] if config else AWS.bucket
        client = boto3.client('s3', aws_access_key_id=config['amazon']['id'],
                              aws_secret_access_key=config['amazon']['secret']) if config else AWS.client
        client.create_bucket(Bucket=bucket)
        return bucket, client

    @staticmethod
    def write(file_url, file_data, config):
        bucket, client = AWS.__get_bucket_client(config)
        client.put_object(Bucket=bucket, Body=file_data, Key=file_url)
        logger.info(f'Amazon write completed at {file_url}')

    @staticmethod
    def read(file_url, config):
        bucket, client = AWS.__get_bucket_client(config)
        return client.get_object(Bucket=bucket, Key=file_url)['Body'].read()

    @staticmethod
    def move(src_url, dst_url, config):
        bucket, client = AWS.__get_bucket_client(config)
        client.copy_object(Bucket=bucket, CopySource={'Bucket': bucket, 'Key': src_url}, Key=dst_url)
        client.delete_object(Bucket=bucket, Key=src_url)

    @staticmethod
    def file_exists(file_url, config):
        bucket, client = AWS.__get_bucket_client(config)
        response = client.list_objects_v2(Bucket=bucket, Prefix=file_url)
        for content in response.get('Contents', []):
            if file_url == content.get('Key'):
                return True
        return False

    @staticmethod
    def get_absolute_path(file_url, config):
        bucket, client = AWS.__get_bucket_client(config)
        return client.generate_presigned_url('get_object', Params={'Bucket': bucket, 'Key': file_url})

    @staticmethod
    def get_files(dir_url, config):
        bucket, client = AWS.__get_bucket_client(config)
        response = client.list_objects_v2(Bucket=bucket, Prefix=dir_url, Delimiter='/')
        for prefix in response.get('Contents', []):
            prefix = prefix.get('Key')
            if prefix != '/':
                yield prefix[len(dir_url):]

    @staticmethod
    def get_dirs(dir_url, config):
        bucket, client = AWS.__get_bucket_client(config)
        dir_url = '' if dir_url == '/' else dir_url     # adjustment for 'root' folder
        response = client.list_objects_v2(Bucket=bucket, Prefix=dir_url, Delimiter='/')
        for prefix in response.get('CommonPrefixes', []):
            prefix = prefix.get('Prefix')
            if prefix != '/':
                yield prefix[len(dir_url):-1]

    if cfg.current_storage == cfg.StorageEnum.AWS:
        bucket, client = __get_bucket_client.__func__(cfg.storage_cfg)
