import io
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
from naming_utils import *
from storage_config import StorageEnum, storage_worker
from zookeeper_utils import Zookeeper, CallBack as ZookeeperCallBack
from storage_utils import Helper
from enumerations import *

import logging
# import logging_util
logger = logging.getLogger('user_input')


MODEL_PREFIX = 'build_'
USER_INPUT_FILE = 'user_input.json'

GOLDEN_TERMS_FOLDER = 'golden_terms'
CONTEXTS_FOLDER = 'context_terms'
SEEDS_FOLDER = 'seed_docs'
GOLDEN_DOCS_FOLDER = 'golden_docs'
SAMPLE_DATA_FOLDER = 'sample_data'
SAMPLE_DATA_ENTITY_FOLDER = 'sample_data_entity'
NARRATIONS = 'narrations'
BASE_ONTOLOGY = 'base_ontology'
DOWNLOAD_KEYS = 'download_keys'
GOLDEN_LINKS_FILE = 'golden_document_links.txt'

class UserInput:
    @staticmethod
    def save_async(data, path_items):
        def save_azure_folders():
            file_paths = []

            def collect_file_paths(key, sub_dir):
                if key not in data or len(list(filter(None, data[key]))) == 0:
                    return

                for value in data[key]:
                    dir_ = json.loads(value)
                    config = {
                        'azure': {
                            'conn_string': dir_.get('conn_string'),
                            'folder': dir_.get('folder'),
                            'container': dir_.get('container'),  # Todo: ShareName --> ContainerName
                        }
                    }
                    for file_name in Storage.get_files(dir_.get('folder'), True, config, StorageEnum.AZURE):
                        unique_name = Naming.custom_name(file_name)
                        dst_path = os.path.join(base_path, sub_dir, unique_name)
                        src_path = os.path.join(dir_.get('folder'), file_name)
                        file_paths.append((src_path, dst_path, config))

            def save_files(file_details):
                src_path, dst_path, config = file_details
                Storage.write(dst_path, Storage.read(src_path, True, config, StorageEnum.AZURE))

            for item in path_items:
                if item == 'golden_terms':
                    collect_file_paths('golden_terms_azure', GOLDEN_TERMS_FOLDER)
                elif item == 'seed_documents':
                    collect_file_paths('seed_documents_azure', SEEDS_FOLDER)
                elif item == 'golden_documents':
                    collect_file_paths('golden_documents_azure', GOLDEN_DOCS_FOLDER)
                elif item == 'sample_data':
                    collect_file_paths('sample_data_azure', SAMPLE_DATA_FOLDER)
                elif item == 'sample_data_entity':
                    collect_file_paths('sample_data_entity_azure', SAMPLE_DATA_FOLDER)
                elif item == 'narrations':
                    collect_file_paths('narrations_azure', NARRATIONS)
                elif item == 'base_ontology':
                    collect_file_paths('base_ontology_azure', BASE_ONTOLOGY)

            with ThreadPoolExecutor(max_workers=storage_worker) as executor:
                executor.map(save_files, file_paths)

        def save_sharepoint_folders():
            def save_files(key, sub_dir):
                for value in data[key]:
                    dir_ = json.loads(value)
                    context = SharePoint.get_context(dir_.get('Username'), dir_.get('Password'), dir_.get('SiteLink'))
                    for file_name, file_url in SharePoint.get_files(context, dir_.get('FolderPath')):
                        path = os.path.join(base_path, sub_dir, file_name)
                        Storage.write(path, SharePoint.read(context, file_url))

            save_files('golden_terms_sharepoint', GOLDEN_TERMS_FOLDER)
            save_files('context_terms_sharepoint', CONTEXTS_FOLDER)
            save_files('seed_documents_sharepoint', SEEDS_FOLDER)
            save_files('golden_documents_sharepoint', GOLDEN_DOCS_FOLDER)
            save_files('sample_data_sharepoint', SAMPLE_DATA_FOLDER)
            save_files('sample_data_entity_sharepoint', SAMPLE_DATA_FOLDER)
            save_files('narrations_sharepoint', NARRATIONS)

        def save_network_folders():
            file_paths = []

            def collect_file_paths(key, sub_dir):
                for value in data[key]:
                    dir_ = json.loads(value)
                    config = {
                        'network': {
                            'host_name': dir_.get('host_name'),
                            'user_name': dir_.get('user_name'),
                            'password': dir_.get('password'),
                        }
                    }
                    for file_name in Storage.get_files(dir_.get('base_url'), True, config, StorageEnum.NETWORK):
                        dst_path = os.path.join(base_path, sub_dir, file_name)
                        src_path = os.path.join(dir_.get('base_url'), file_name)
                        file_paths.append((src_path, dst_path, config))

            def save_files(file_details):
                src_path, dst_path, config = file_details
                Storage.write(dst_path, Storage.read(src_path, True, config, StorageEnum.NETWORK))

            for item in path_items:
                if item == 'golden_terms':
                    collect_file_paths('golden_terms_local', GOLDEN_TERMS_FOLDER)
                elif item == 'context_terms':
                    collect_file_paths('context_terms_local', CONTEXTS_FOLDER)
                elif item == 'seed_documents':
                    collect_file_paths('seed_documents_local', SEEDS_FOLDER)
                elif item == 'golden_documents':
                    collect_file_paths('golden_documents_local', GOLDEN_DOCS_FOLDER)
                elif item == 'sample_data':
                    collect_file_paths('sample_data_local', SAMPLE_DATA_FOLDER)
                elif item == 'sample_data_entity':
                    collect_file_paths('sample_data_entity_local', SAMPLE_DATA_FOLDER)
                elif item == 'narrations':
                    collect_file_paths('narrations_local', NARRATIONS)
            with ThreadPoolExecutor(max_workers=storage_worker) as executor:
                executor.map(save_files, file_paths)

        base_path = ModuleEnum.USER_INPUT.value
        save_azure_folders()
        Naming.write()

    @staticmethod
    def save(data, path_items):
        def save_local_files():
            file_paths = []

            def collect_file_paths(key, sub_dir):
                files = data[key]
                copied_file_paths = []
                for file in files:
                    unique_name = Naming.custom_name(file.filename)
                    dst_path = os.path.join(base_path, sub_dir, unique_name)
                    copied_file_paths.append(dst_path)
                    file_paths.append((file, dst_path))

            def save_files(file_details):
                file, dst_path = file_details
                Storage.write(dst_path, file.read())

            for item in path_items:
                if item == 'golden_terms':
                    collect_file_paths('golden_terms_local', GOLDEN_TERMS_FOLDER)
                elif item == 'seed_documents':
                    collect_file_paths('seed_documents_local', SEEDS_FOLDER)
                elif item == 'golden_documents':
                    collect_file_paths('golden_documents_local', GOLDEN_DOCS_FOLDER)
                elif item == 'sample_data':
                    collect_file_paths('sample_data_local', SAMPLE_DATA_FOLDER)
                elif item == 'sample_data_entity':
                    collect_file_paths('sample_data_entity_local', SAMPLE_DATA_FOLDER)
                elif item == 'narrations':
                    collect_file_paths('narrations_local', NARRATIONS)
                elif item == 'base_ontology':
                    collect_file_paths('base_ontology_local', BASE_ONTOLOGY)

            with ThreadPoolExecutor(max_workers=storage_worker) as executor:
                executor.map(save_files, file_paths)

        download = data.get('download') if data.get('download') else 'true'
        Zookeeper.write(ModuleEnum.CONFIG, AttributeEnum.DOWNLOAD, json.loads(download.lower()))

        type_ = data.get('golden_documents_type') if data.get('golden_documents_type') else GoldenDocumentType.RAW.value
        Zookeeper.write(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_DOCUMENTS_TYPE, type_)

        base_path = ModuleEnum.USER_INPUT.value
        save_local_files()
        Naming.write()

        golden_document_links = data.get('selected_golden_document_links')
        if golden_document_links:
            golden_document_links = [a for link in golden_document_links for a in link.split(',') ]
            Storage.write(os.path.join(base_path,'golden_document_links.txt'), bytes('\n'.join(golden_document_links), 'utf-8'))


    @staticmethod
    def should_download():
        return Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.DOWNLOAD)

    @staticmethod
    def has_golden_terms():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, GOLDEN_TERMS_FOLDER)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def get_golden_terms_files():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, GOLDEN_TERMS_FOLDER)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]

    @staticmethod
    def has_contexts():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, CONTEXTS_FOLDER)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def get_contexts_docs():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, CONTEXTS_FOLDER)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]

    @staticmethod
    def has_seed_docs():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, SEEDS_FOLDER)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def has_golden_documents():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, GOLDEN_DOCS_FOLDER)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def get_golden_documents():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, GOLDEN_DOCS_FOLDER)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]

    @staticmethod
    def has_golden_document_links() -> bool:
        file_ = os.path.join(ModuleEnum.USER_INPUT.value, GOLDEN_LINKS_FILE)
        try:
            Storage.read(file_)
            return True
        except:
            return False

    @staticmethod
    def get_golden_document_links() -> list:
        file_ = os.path.join(ModuleEnum.USER_INPUT.value, GOLDEN_LINKS_FILE)
        try:
            return str(Storage.read(file_), 'utf-8').splitlines()
        except:
            return []

    @staticmethod
    def get_seed_docs():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, SEEDS_FOLDER)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]

    @staticmethod
    def has_training_sample_data():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, SAMPLE_DATA_FOLDER)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def has_training_sample_data_entity():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, SAMPLE_DATA_ENTITY_FOLDER)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def get_training_sample_data_entity():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, SAMPLE_DATA_ENTITY_FOLDER)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]

    @staticmethod
    def get_training_sample_data():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, SAMPLE_DATA_FOLDER)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]

    @staticmethod
    def has_narrations():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, NARRATIONS)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def get_narrations():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, NARRATIONS)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]

    @staticmethod
    def has_base_ontology():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, BASE_ONTOLOGY)
        return len(list(Storage.get_files(dir_))) > 0

    @staticmethod
    def get_base_ontology():
        dir_ = os.path.join(ModuleEnum.USER_INPUT.value, BASE_ONTOLOGY)
        return [os.path.join(dir_, file) for file in Storage.get_files(dir_)]
