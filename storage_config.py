from enum import Flag, auto


class StorageEnum(Flag):
    LOCAL = auto()
    AZURE = auto()
    AWS = auto()
    GCP = auto()
    NETWORK = auto()


storage_worker = 16
current_storage = StorageEnum.LOCAL | StorageEnum.AZURE
storage_log_file = 'logs/storage_logs.log'
map_file_names = 'filename_mappings.json'
storage_cfg = {
    'local': {
        'folder': '/home/parabole/automation/learning'
    },
    'aws': {
        'id': 'AKIAW6OOAHKCXTTKDLJK',
        'secret': '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',
        'bucket': 'parabole',
        'folder': 'automation/learning'
    },
    'azure': {
        'conn_string': 'DefaultEndpointsProtocol=https;AccountName=storageashish2019;AccountKey=aOd8wf8Fn650f9'
                       'MPpfL0lByl0NYJRP1uwacYiT2vTbYjsv0woamAL2X7fTMAqe+e2/nEbHCfh13P6JbX0GJewg==;EndpointSuff'
                       'ix=core.windows.net',
        'container': 'parabole',
        'folder': 'automation/learning'
    },
    'gcp': {
        'project_id': 'bert-er',
        'bucket': 'bert-er',
        'folder': 'automation/learning'
    },
    'network': {
        'host_name': '172.16.214.22',
        'user_name': 'parabole',
        'password': 'Dur01nag',
        'folder': 'automation/learning'
    }
}
