from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.file import File


class SharePoint:
    @staticmethod
    def is_valid_credentials(username, password, site_link):
        ctx_auth = AuthenticationContext(site_link)
        xx = ctx_auth.acquire_token_for_user(username, password)
        print(ctx_auth.provider)
        print(xx)
        ctx = ClientContext(site_link, ctx_auth)
        print(ctx)
        print(ctx_auth.get_last_error() == '')
        return ctx

    @staticmethod
    def is_valid_folder(context, dir_url):
        # get directory object
        list_obj = context.web.lists.get_by_title(dir_url)
        folder = list_obj.root_folder
        context.load(folder)
        context.execute_query()

        # get files in the directory
        files = folder.files
        context.load(files)
        context.execute_query()

    @staticmethod
    def get_context(username, password, site_link):
        ctx_auth = AuthenticationContext(site_link)
        if ctx_auth.acquire_token_for_user(username, password):
            return ClientContext(site_link, ctx_auth)
        else:
            raise Exception('Invalid credentials provided.')

    @staticmethod
    def get_files(context, dir_url):
        dir_files = []

        # get directory object
        list_obj = context.web.lists.get_by_title(dir_url)
        folder = list_obj.root_folder
        context.load(folder)
        context.execute_query()

        # get files in the directory
        files = folder.files
        context.load(files)
        context.execute_query()
        for cur_file in files:
            name = cur_file.properties["Name"]
            file_url = cur_file.properties['ServerRelativeUrl']
            dir_files.append((name, file_url))
        return dir_files

    @staticmethod
    def read(context, file_url):
        return File.open_binary(context, file_url).content
