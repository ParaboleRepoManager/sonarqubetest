import os
import json
import storage_config as cfg
from enumerations import ModuleEnum, AttributeEnum, GoldenLinkType
from zookeeper_utils import Zookeeper
import zk_config
# from utils.storage_azure import Azure
from storage_aws import AWS
from storage_local import Local
from storage_network import Network
import logging

logger = logging.getLogger(__name__)


class Storage:
    @staticmethod
    def get_storage(storage=None, use_disk=False):
        storage = storage if storage else cfg.current_storage
        if cfg.StorageEnum.LOCAL in storage:
            if use_disk:
                if cfg.StorageEnum.AZURE in storage:
                    return cfg.StorageEnum.AZURE
                if cfg.StorageEnum.AWS in storage:
                    return cfg.StorageEnum.AWS
                if cfg.StorageEnum.GCP in storage:
                    return cfg.StorageEnum.GCP
                if cfg.StorageEnum.NETWORK in storage:
                    return cfg.StorageEnum.NETWORK
            return cfg.StorageEnum.LOCAL
        return storage

    @staticmethod
    def get_base_prefix(storage=None, use_disk=False):
        store = Storage.get_storage(storage, use_disk).name.lower()
        return os.path.join(cfg.storage_cfg[store]["folder"], zk_config.zk_deployment_path)

    @staticmethod
    def get_storage_prefix(storage=None, use_disk=False):
        store = Storage.get_storage(storage, use_disk).name.lower()
        return os.path.join(cfg.storage_cfg[store]["folder"], Zookeeper.get_build_prefix())

    @staticmethod
    def __normalize_path(url, is_absolute, storage, config, from_base=False, iteration=None):
        if is_absolute:
            return url.replace('\\', '/')
        iter_ = '' if from_base else f'iteration_{Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)}'
        iter_ = iter_ if iteration is None else iteration
        return os.path.normpath(os.path.join(Storage.get_storage_prefix(storage), iter_, url)).replace('\\', '/')

    @staticmethod
    def write(file_url, file_data, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, from_base=from_base)

        if storage == cfg.StorageEnum.LOCAL:
            Local.write(file_url, file_data)
        elif storage == cfg.StorageEnum.AWS:
            AWS.write(file_url, file_data, config)
        elif storage == cfg.StorageEnum.AZURE:
            Azure.write(file_url, file_data, config)
        elif storage == cfg.StorageEnum.NETWORK:
            return Network.write(file_url, file_data, config)

    @staticmethod
    def read(file_url, is_absolute=False, config=None, storage=None, iteration=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, iteration=iteration, from_base=from_base)
        if storage == cfg.StorageEnum.LOCAL:
            return Local.read(file_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.read(file_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.read(file_url, config)
        elif storage == cfg.StorageEnum.NETWORK:
            return Network.read(file_url, config)

    @staticmethod
    def file_exists(file_url, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, from_base=from_base)

        if storage == cfg.StorageEnum.LOCAL:
            return Local.file_exists(file_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.file_exists(file_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.file_exists(file_url, config)

    @staticmethod
    def move(src_url, dst_url, is_absolute=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        src_url = Storage.__normalize_path(src_url, is_absolute, storage, config)
        dst_url = Storage.__normalize_path(dst_url, is_absolute, storage, config)

        if storage == cfg.StorageEnum.LOCAL:
            Local.move(src_url, dst_url)
        elif storage == cfg.StorageEnum.AWS:
            AWS.move(src_url, dst_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            Azure.move(src_url, dst_url, config)

    @staticmethod
    def push_local(src_absolute_url_local, dst_url, is_absolute=False, config=None, storage=None, delete_local=True, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dst_url = Storage.__normalize_path(dst_url, is_absolute, storage, config)

        src_files = [src_absolute_url_local]
        dst_urls = [dst_url]
        if Local.is_dir(src_absolute_url_local):
            files = Local.get_files(src_absolute_url_local, recursive=True)
            src_files = [os.path.join(src_absolute_url_local, file) for file in files]
            dst_urls = [os.path.join(dst_url, file) for file in files]

        for src_file, dst_url in zip(src_files, dst_urls):
            content = Local.read(src_file)
            if storage == cfg.StorageEnum.LOCAL:
                Local.write(dst_url, content)
            elif storage == cfg.StorageEnum.AWS:
                AWS.write(dst_url, content, config)
            elif storage == cfg.StorageEnum.AZURE:
                Azure.write(dst_url, content, config)

            if delete_local:
                Local.delete(src_file)    # remove the local version of file

    @staticmethod
    def pull_remote(src_url, dst_absolute_url_local, is_absolute=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        src_url = Storage.__normalize_path(src_url, is_absolute, storage, config)
        if dst_absolute_url_local is None or dst_absolute_url_local == '':
            current_dir = os.path.dirname(os.path.realpath(__file__))
            dst_absolute_url_local = os.path.join(current_dir, os.path.basename(src_url))
            print(dst_absolute_url_local)

        dir_ = os.path.dirname(dst_absolute_url_local)
        if not dir_:
            current_dir = os.path.dirname(os.path.realpath(__file__))
            dst_absolute_url_local = os.path.join(current_dir, dst_absolute_url_local)
            print('empty')

        if storage == cfg.StorageEnum.LOCAL:
            if Local.file_exists(src_url):
                Local.write(dst_absolute_url_local, Local.read(src_url))
            else:
                src_files = Local.get_files(src_url)  # todo: recursive=True
                for src_file in src_files:
                    dst_file = os.path.join(dst_absolute_url_local, src_file)
                    os.makedirs(os.path.dirname(dst_file), exist_ok=True)
                    Local.write(dst_file, Local.read(os.path.join(src_url, src_file)))
        elif storage == cfg.StorageEnum.AWS:
            Local.write(dst_absolute_url_local, AWS.read(src_url, config))
        elif storage == cfg.StorageEnum.AZURE:
            if Azure.file_exists(src_url, config):
                Local.write(dst_absolute_url_local, Azure.read(src_url, config))
            else:
                src_files = Azure.get_files(src_url, config, recursive=True)
                for src_file in src_files:
                    dst_file = os.path.join(dst_absolute_url_local, src_file)
                    os.makedirs(os.path.dirname(dst_file), exist_ok=True)
                    Local.write(dst_file, Azure.read(os.path.join(src_url, src_file), None))
        elif storage == cfg.StorageEnum.NETWORK:
            Local.write(dst_absolute_url_local, Network.read(src_url, config))

    @staticmethod
    def get_absolute_path(file_url, from_base=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        return Storage.__normalize_path(file_url, False, storage, config, from_base=from_base)

    @staticmethod
    def remove_prefix(text, prefix):
        if text.startswith(prefix):
            return text[len(prefix):]
        return text

    @staticmethod
    def get_relative_path(file_url, from_base=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        iter_ = f'iteration_{Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)}'
        # relative_path = os.path.normpath(file_url).removeprefix(os.path.join(Storage.get_storage_prefix(storage), iter_))
        prefix = os.path.join(Storage.get_storage_prefix(storage), iter_)
        relative_path = Storage.remove_prefix(os.path.normpath(file_url), os.path.normpath(prefix))
        if relative_path == os.path.normpath(file_url):
            return None
        return relative_path[1:]

    @staticmethod
    def create_absolute_path(org, domain, sub_domain, build_name, build_type, iter_, file_url):
        store = Storage.get_storage(None, False).name.lower()
        folder = cfg.storage_cfg[store]["folder"]
        prefix = Zookeeper.get_build_prefix(org, domain, sub_domain, build_name, build_type)
        return os.path.join(folder, prefix, iter_, file_url)

    @staticmethod
    def get_external_path(file_url, is_absolute=False, from_base=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, from_base=from_base)

        if storage == cfg.StorageEnum.LOCAL:
            return Local.get_absolute_path(file_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_absolute_path(file_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.get_external_path(file_url, config)

    @staticmethod
    def get_files(dir_url, is_absolute=False, config=None, storage=None, iteration=None, recursive=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dir_url = Storage.__normalize_path(dir_url, is_absolute, storage, config, iteration=iteration) + '/'

        if storage == cfg.StorageEnum.LOCAL:
            return Local.get_files(dir_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_files(dir_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            ss = Azure.get_files(dir_url, config, recursive=recursive)
            return ss
        elif storage == cfg.StorageEnum.NETWORK:
            return Network.get_files(dir_url, config)

    @staticmethod
    def get_dirs(dir_url, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dir_url = Storage.__normalize_path(dir_url, is_absolute, storage, config, from_base) + '/'
        if storage == cfg.StorageEnum.LOCAL:
            return Local.get_dirs(dir_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_dirs(dir_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.get_dirs(dir_url, config)

    @staticmethod
    def get_model_iterations(only_previous=False):
        iterations = list(Storage.get_dirs(Storage.get_storage_prefix(), from_base=True))
        if only_previous:
            current_iteration = f'iteration_{Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)}'
            iterations.remove(current_iteration)
        return iterations

    @staticmethod
    def clone_dir(src_dir, dst_dir, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        src_dir = Storage.__normalize_path(src_dir, is_absolute, storage, config, from_base) + '/'
        dst_dir = Storage.__normalize_path(dst_dir, is_absolute, storage, config, from_base) + '/'
        if storage == cfg.StorageEnum.LOCAL:
            return Local.clone_dir(src_dir, dst_dir)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.clone_dir(src_dir, dst_dir, config)

    @staticmethod
    def delete_dir(dir_url, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dir_url = os.path.join(dir_url, '')
        dir_url = Storage.__normalize_path(dir_url, is_absolute, storage, config, from_base)
        if storage == cfg.StorageEnum.LOCAL:
            return Local.delete_dir(dir_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_dirs(dir_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.delete_dir(dir_url, config)

    @staticmethod
    def exec_command(command, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        if storage == cfg.StorageEnum.NETWORK:
            return Network.exec_command(command, config)

    @staticmethod
    def get_local_tmp_dir(module: ModuleEnum, sub_dir=''):
        return os.path.join(cfg.storage_cfg['local']['folder'], module.value, sub_dir)

    @staticmethod
    def get_file_path(build=None):
        config = cfg.storage_cfg
        storage = Storage.get_storage()
        store = 'azure' if storage == cfg.StorageEnum.AZURE else \
            'aws' if storage == cfg.StorageEnum.AWS else \
                'local' if storage == cfg.StorageEnum.LOCAL else \
                    'network' if storage == cfg.StorageEnum.NETWORK else ''
        folder = config[store]["folder"]
        build = build if build else Zookeeper.read(ModuleEnum.USER_INPUT, AttributeEnum.MODEL)
        return os.path.join(folder, zk_config.zk_root_path, build, 'zk_nodes.json')

class Helper:
    @staticmethod
    def get_golden_terms(reviewed: bool=True) -> list:
        terms = set()
        if not reviewed:   # fetch non-reviewed terms aka terms
            try:
                path = os.path.join(Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERMS), 'terms.gol')
                terms = terms.union(str(Storage.read(path), 'utf-8').splitlines())
            except FileNotFoundError: pass
            return list(terms)

        iterations = Storage.get_model_iterations()    # fetch reviewed golden terms from all iterations
        for i in iterations:
            path = os.path.join(Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERMS), 'input_terms.gol')
            try:
                terms = terms.union(str(Storage.read(path, iteration=i), 'utf-8').splitlines())
            except FileNotFoundError: pass
        return list(terms)

    @staticmethod
    def set_golden_terms(terms: list, reviewed: bool=True):
        terms_ = set(terms)
        file_name = 'input_terms.gol' if reviewed else 'terms.gol'
        path = os.path.join(Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERMS), file_name)
        try:
            terms_ = terms_.union(str(Storage.read(path), 'utf-8').splitlines())
        except FileNotFoundError: pass
        print(path)
        Storage.write(path, bytes("\n".join(terms_), encoding='utf-8'))

    @staticmethod
    def get_golden_doc_urls(model_id=None):
        document_links = []
        path = os.path.join(cfg.storage_cfg[Storage.get_storage().name.lower()]["folder"], Zookeeper.get_build_path(), 'golden_document_list.json')
        try:
            file_links = {}
            try:
                file_links = json.loads(Storage.read(path, is_absolute=True))
            except: pass
            if model_id == '':
                model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)
            for unparsed_link, info in file_links.items():
                m_id = info.get('model_id')
                if model_id is None or model_id == m_id:
                    external_link = info.get(GoldenLinkType.PUBLIC)
                    external_link = external_link if external_link else Storage.get_external_path(unparsed_link, is_absolute=True)
                    document_links.append((unparsed_link, external_link, m_id))
        except FileNotFoundError: pass
        return document_links

    @staticmethod
    def get_golden_doc_paths(model_id=None, relative=True) -> list:
        document_links = set()
        path = os.path.join(cfg.storage_cfg[Storage.get_storage().name.lower()]["folder"], Zookeeper.get_build_path(), 'golden_document_list.json')
        try:
            file_links = {}
            try:
                file_links = json.loads(Storage.read(path, is_absolute=True))
            except: pass
            if model_id == '':
                model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)

            for unparsed_link in file_links.keys():
                info = file_links.get(unparsed_link, {})
                if model_id is None or model_id == info.get('model_id'):
                    document_links.add(unparsed_link)
        except FileNotFoundError: pass
        if relative:
            document_links = [Storage.get_relative_path(doc_link) for doc_link in document_links]
            document_links = [doc_link for doc_link in document_links if doc_link]  # revone all None
        return list(document_links)

    @staticmethod
    def get_golden_doc_sentences(unparsed_links=None) -> list:
        document_links = set()
        path = os.path.join(cfg.storage_cfg[Storage.get_storage().name.lower()]["folder"], Zookeeper.get_build_path(), 'golden_document_list.json')
        try:
            file_links = {}
            try:
                file_links = json.loads(Storage.read(path, is_absolute=True))
            except: pass
            if model_id == '':
                model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)

            for unparsed_link in file_links.keys():
                info = file_links.get(unparsed_link, {})
                sentence_link = info.get(GoldenLinkType.SENTENCES)
                if unparsed_links is None or (unparsed_link in unparsed_links and sentence_link):
                    document_links.add(sentence_link)
        except FileNotFoundError: pass
        return list(document_links)

    @staticmethod
    def set_golden_doc_links(links: dict, link_type=GoldenLinkType.PUBLIC):
        path = os.path.join(cfg.storage_cfg[Storage.get_storage().name.lower()]["folder"], Zookeeper.get_build_path(), 'golden_document_list.json')
        file_links = {}
        try:
            file_links = json.loads(Storage.read(path, is_absolute=True))
        except: pass
        model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)
        for unparsed_link, link_to_add in links.items():
            info = file_links.get(unparsed_link, {})
            info['model_id'] = info.get('model_id', model_id)
            if link_type != GoldenLinkType.UNPARSED:
                info[link_type] = link_to_add
            file_links[unparsed_link] = info
        Storage.write(path, bytes(json.dumps(file_links), encoding='utf-8'), is_absolute=True)

    @staticmethod
    def set_context(name, description):
        path = os.path.join(cfg.storage_cfg[Storage.get_storage().name.lower()]["folder"], Zookeeper.get_build_path(), 'contexts.json')
        contexts = {}
        try:
            contexts = json.loads(Storage.read(path, is_absolute=True))
        except: pass
        context = contexts.get(name, {})

        context['description'] = description if description else ''
        context['version'] = str(float(context.get('version', '0.0'))+1)
        models = context.get('models', {})
        models[context['version']] = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)
        context['models'] = models

        contexts[name] = context
        Storage.write(path, bytes(json.dumps(contexts), encoding='utf-8'), is_absolute=True)

    @staticmethod
    def get_contexts():
        path = os.path.join(cfg.storage_cfg[Storage.get_storage().name.lower()]["folder"], Zookeeper.get_build_path(), 'contexts.json')
        contexts = {}
        try:
            contexts = json.loads(Storage.read(path, is_absolute=True))
        except: pass
        return [(name, context.get('description'), context.get('version'), context.get('models')) for name, context in contexts.items()]

    @staticmethod
    def get_model_links(build_id, model_id, model_type):
        pass


#Zookeeper.create_build_nodes('org_1', 'domain_1', 'subdomain_1', 'build_20', 'unsupervised', None)
#print(Helper.get_golden_terms(False))
#Helper.set_golden_doc_links({'i':'j', 'k': 'l'}, link_type=GoldenLinkType.UNPARSED, iter=999)
#print(Helper.get_golden_doc_links(GoldenLinkType.UNPARSED, iter=999))
#Helper.set_golden_terms(['hh', 'b', 'c', 'x'])
#Helper.set_golden_terms(['xcccccccccc'], False)
#print(Helper.get_golden_terms())
