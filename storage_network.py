import paramiko
import storage_config as cfg
import logging
#import logging_util

logger = logging.getLogger(__name__)


class Network:
    @staticmethod
    def __get_client(config):
        if not config:
            return Network.client
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(config["network"]["host_name"],
                       banner_timeout=200,
                       username=config["network"]["user_name"],
                       password=config["network"]["password"])
        return client

    @staticmethod
    def read(file_url, config):
        client = Network.__get_client(config)
        sftp_client = client.open_sftp()
        with sftp_client.file(file_url, mode='rb') as file:
            file.prefetch(file.stat().st_size)
            return file.read()

    @staticmethod
    def write(file_url, file_data, config):
        client = Network.__get_client(config)
        sftp_client = client.open_sftp()
        with sftp_client.file(file_url, mode='wb') as file:
            file.write(file_data)

    @staticmethod
    def get_files(dir_url, config):
        client = Network.__get_client(config)
        stdin, stdout, stderr = client.exec_command('ls -l ' + dir_url)
        for line in stdout:
            line = line.strip('\n')
            if line.startswith('-'):
                yield line.split()[-1]

    @staticmethod
    def exec_command(command, config):
        client = Network.__get_client(config)
        stdin, stdout, stderr = client.exec_command(command)
        output = ""
        for line in stdout:
            output += line
        return output

    if cfg.current_storage == cfg.StorageEnum.NETWORK:
        client = __get_client.__func__(cfg.storage_cfg)